export class ICustomer{

    public cid?:number;
    public fname:string;
    public sname:string;
    public password?:string;
    public phone:string;
    public username:string;
    public gender:string;
    public email:string;
    public image?:string;
    public role:string;
    public balance?:string;
    public referralCode?:string;
    public accountName?:string;
    public accountNumber?:number;
    public accountBank?:string;
    public dropper?:any;
}