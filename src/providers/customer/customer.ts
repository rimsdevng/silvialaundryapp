import {Injectable} from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';
import {Ilocation} from "../../interfaces/location.interface";
import {FileTransferObject, FileTransfer} from "@ionic-native/file-transfer";
import {IDelivery} from "../../interfaces/delivery.interface";
import {ICustomer} from "../../interfaces/customer.interface";
import { Storage } from "@ionic/storage";
import { BackgroundGeolocation , BackgroundGeolocationConfig, BackgroundGeolocationResponse} from '@ionic-native/background-geolocation';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import 'rxjs/add/operator/filter';
import {isUndefined} from "ionic-angular/util/util";

/*
  Generated class for the CustomerProvider provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular DI.
*/
declare let Bugsnag;
Bugsnag.apiKey = "d06999de6b1609640f6787d276f7825f";

@Injectable()
export class CustomerProvider {

     // public url:string = 'http://dropster.gurudeveloperinc.com/';
    public url:string = 'http://localhost:8000/';
    public isFinishedRequesting:boolean = false;
    public lastDid:number;
    public customer:ICustomer;
    public watch: any;
    public lat: number = 0;
    public lng: number = 0;
    public showDropperMenu:boolean = false;
    public showVpMenu:boolean = false;

    constructor(public http: Http,
          private transfer: FileTransfer,
          public geolocation: Geolocation,
          public backgroundGeolocation: BackgroundGeolocation,
          public storage:Storage

    ) {

        this.storage.get('customer').then((value)=>{
            this.customer = value;
        });

    }


    getPrice(from:Ilocation,to:Ilocation,vcid){
      console.log('https://maps.googleapis.com/maps/api/distancematrix/json?origins='
          + from.lat + ',' + from.lng +
          '&destinations=' + to.lat + ',' + to.lng +
          '&key=%20AIzaSyAu__GmIFvCZSsnI6WP6OKu_TVvRYQ75i8');

        let params = {
            'fromLat' : from.lat,
            'fromLng' : from.lng,
            'toLat'   : to.lat,
            'toLng'   : to.lng,
            'vcid'    : vcid
        };

        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(params);
        return this.http.post(this.url + 'api/price', body, options).map((res: Response) => res.json());


    }

    signUp(customer) {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(customer);
        return this.http.post(this.url + 'api/sign-up', body, options).map((res: Response) => res.json());
    }

    socialSignUp(customer) {
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(customer);
        return this.http.post(this.url + 'api/social-sign-up', body, options).map((res: Response) => res.json());
    }

    signIn(customer){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(customer);
        return this.http.post(this.url + 'api/sign-in', body, options).map((res: Response) => res.json());

    }

    forgotPassword(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/forgot-password',body,options).map((res:Response) => res.json());

    }

    socialSignIn(customer){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(customer);
        return this.http.post(this.url + 'api/social-sign-in',body,options).map((res:Response) => res.json());

    }

    confirmVerification(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/confirm-verification',body,options).map((res:Response) => res.json());

    }


    availableDeliveries(){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/available-requests',options).map((res:Response) => res.json());
    }

    deliveriesInProgress(){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/deliveries-in-progress/' + this.customer.cid , options).map((res:Response) => res.json());
    }


    deliveryInProgress(did){
        let deliveryid;
        if(did <= 0) deliveryid = this.lastDid;
        else deliveryid = did;
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/delivery-in-progress/' + deliveryid,options).map((res:Response) => res.json());
    }

    deliveryItems(){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/delivery-items', options).map((res:Response) => res.json());
    }

    newDelivery(delivery:IDelivery) {


        return new Promise((resolve,reject) => {

        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(delivery);
        this.http.post(this.url + 'api/new-delivery', body, options).map((res: Response) => res.json())
            .subscribe((success:string)=>{

            this.lastDid = Number.parseInt(success);


                let option:any = {
                    fileKey: "image",
                    fileName: "image.jpg",
                    chunkedMode: false,
                    mimeType: "multipart/form-data",
                    params : {did: success},
                };

                const fileTransfer: FileTransferObject = this.transfer.create();


                let count = 0;
                if(count == delivery.images.length){
                    this.isFinishedRequesting =  true;
                }


                // Use the FileTransfer to upload the image
                for(let i = 0; i < delivery.images.length; i++){
                    fileTransfer.upload(delivery.images[i] , this.url + 'api/add-delivery-image', option).then(data => {
                        console.log(data);
                        count++;
                        if(count == delivery.images.length){
                           this.isFinishedRequesting =  true;
                        }
                    }, err => {
                        count++;
                        console.log("image " + count + " didn't upload");
                        console.log(err);
                        if(count == delivery.images.length){
                            this.isFinishedRequesting =  true;
                        }
                    });

                    console.log('uploading image ' + i);
                }



                resolve(success);



            },(error)=>{

                console.log(error);
                Bugsnag.notifyException(new Error("New Delivery Custom Error"), {
                    "Error Details": {
                        error:error
                    }
                });

                reject(error);
        });

        });

    }

    cancelDelivery(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/cancel-delivery',body,options).map((res:Response) => res.json());
    }

    paymentCollected(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/payment-collected',body,options).map((res:Response) => res.json());
    }


    sentDeliveries(){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/customer-sent-deliveries/' + this.customer.cid,options).map((res:Response) => res.json());

    }

    receivedDeliveries(){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/customer-received-deliveries/' + this.customer.cid,options).map((res:Response) => res.json());

    }

    acceptDelivery(dropper){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(dropper);
        return this.http.post(this.url + 'api/accept-delivery',body,options).map((res:Response) => res.json());

    }

    pickedUpDelivery(did){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/pickedup-delivery/' + did,options).map((res:Response) => res.json());
    }

    droppedOffDelivery(did){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/droppedoff-delivery/' + did,options).map((res:Response) => res.json());
    }

    returnedDelivery(did){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/returned-delivery/' + did,options).map((res:Response) => res.json());
    }

    deliveryCompleted(did){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/delivery-completed/' + did,options).map((res:Response) => res.json());
    }

    deliveryIssue(did){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/delivery-issue/' + did,options).map((res:Response) => res.json());
    }

    postDropperLocation(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/dropper-location',body,options).map((res:Response) => res.json());

    }

    addCoupon(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/add-coupon',body,options).map((res:Response) => res.json());

    }

    getDropperLocation(drid){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/dropper-location/' + drid,options).map((res:Response) => res.json());
    }

    dropperDeliveryHistory(){

    }

    startTracking() {
        console.log('Starting to track');
        // Foreground Tracking

        let options = {
            frequency: 15000,
            enableHighAccuracy: true
        };

        let shouldPost = true;

        this.watch = this.geolocation.watchPosition(options).subscribe((position: Geoposition) => {

            if(this.customer !== null) {
                // console.log(position);
                let data = {
                    'cid': this.customer.cid,
                    'lat': position.coords.latitude,
                    'lng': position.coords.longitude
                };

                if(shouldPost){
                    shouldPost = false;
                    // console.log('Posting dropper location - Foreground');
                    this.postDropperLocation(data).subscribe((success) => {
                        // console.log(success);
                        // console.log('Can post now');
                        shouldPost = true;
                    }, (error) => {
                        shouldPost = true;
                        // console.log(error);
                    });

                }

                this.lat = position.coords.latitude;
                this.lng = position.coords.longitude;
            }

        });


        // Background Tracking

        let config : BackgroundGeolocationConfig = {
            desiredAccuracy: 0,
            stationaryRadius: 10,
            distanceFilter: 10,
            debug: false,
            notificationTitle: "DROPSTER",
            notificationText: "Monitoring Enabled",
            interval: 15000
        };

        this.backgroundGeolocation.configure(config).subscribe((location :  BackgroundGeolocationResponse) => {

            console.log('BackgroundGeolocation:  ' + location.latitude + ',' + location.longitude);

            let data = {
                'cid' : this.customer.cid,
                'lat' : location.latitude,
                'lng' : location.longitude
            };

            let shouldPost = true;

            if(shouldPost){
                shouldPost = false;
                this.postDropperLocation(data).subscribe((success)=>{
                    console.log(success);
                    shouldPost = true;
                },(error)=>{

                    shouldPost = true;
                    console.log(error);
                });

            }

            this.lat = location.latitude;
            this.lng = location.longitude;


        }, (err) => {

            console.log(err);

        });

        // Turn ON the background-geolocation system.
        this.backgroundGeolocation.start();



    }

    stopTracking() {
        console.log('stopTracking');

        this.backgroundGeolocation.finish();
        if (this.watch.hasSubscription && this.watch !== null) this.watch.unsubscribe();

    }

    customersUpdatedDetails(cid){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/customers-updated-details/' + cid,options).map((res:Response) => res.json());

    }

    updateCustomer(){
        console.log('updating customer on provider');
        if(isUndefined(this.customer) || this.customer === null){
            this.storage.get('customer').then((value)=>{
                this.customer = value;
            });

        } else {
            this.customersUpdatedDetails(this.customer.cid).subscribe((success)=>{
                this.customer = success;
                Bugsnag.user = {
                    id: this.customer.cid,
                    name: this.customer.fname + " " + this.customer.sname,
                    email: this.customer.email
                };

                if(this.customer.role == "Dropper"){
                    this.showDropperMenu = true;
                    console.log("This is a dropper so we would track");
                    this.startTracking();
                } else {
                    this.stopTracking();
                }


                if(this.customer.role == "Vehicle Partner"){
                    this.showVpMenu = true;
                }

                this.storage.set('customer',success);
            },(error)=>{ console.log(error) });

        }
    }

    earnings(cid){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/earnings/' + cid,options).map((res:Response) => res.json());
    }

    withdrawals(cid){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/withdrawals/' + cid,options).map((res:Response) => res.json());
    }

    customerPayments(cid){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/customer-payments/' + cid,options).map((res:Response) => res.json());
    }

    updateProfile(customer){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(customer);
        return this.http.post(this.url + 'api/update-profile',body,options).map((res:Response) => res.json());

    }


    resolveBankAccountname(bankCode,accountNumber){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get('https://api.paystack.co/bank/resolve?account_number=' + accountNumber
            + '&bank_code=' + bankCode ,options).map((res:Response) => res.json());
    }

    updateBankDetails(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/update-bank-details',body,options).map((res:Response) => res.json());
    }

    changePassword(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/change-password',body,options).map((res:Response) => res.json());
    }

    referred(cid){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/referred/' + cid,options).map((res:Response) => res.json());
    }

    settings(){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/settings/' + this.customer.cid,options).map((res:Response) => res.json());
    }

    changePayout(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/change-payout',body,options).map((res:Response) => res.json());
    }

    getFacebookUser(userid,accessToken){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get('https://graph.facebook.com/' + userid + '?access_token=' + accessToken
            + '&date_format=U&fields=id,first_name,last_name,gender,picture,email' ,options).map((res:Response) => res.json());
    }

    sendPhoneVerification(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/send-phone-verification',body,options).map((res:Response) => res.json());
    }

    confirmPhoneVerification(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/confirm-phone-verification',body,options).map((res:Response) => res.json());
    }

    saveLocation(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/add-customer-location',body,options).map((res:Response) => res.json());

    }

    addFavorite(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/add-favorite',body,options).map((res:Response) => res.json());

    }

    editFavorite(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/edit-favorite',body,options).map((res:Response) => res.json());
    }

    deleteFavorite(data){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        let body = JSON.stringify(data);
        return this.http.post(this.url + 'api/delete-favorite',body,options).map((res:Response) => res.json());
    }


    getFavorites(){
        let headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({headers: headers});
        return this.http.get(this.url + 'api/favorites/' + this.customer.cid,options).map((res:Response) => res.json());
    }

}
