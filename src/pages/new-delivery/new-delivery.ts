import {Component} from '@angular/core';
import {
    ActionSheetController,
    AlertController, IonicPage, Loading, LoadingController, NavController, NavParams,
    Platform
} from 'ionic-angular';
import {Camera, CameraOptions} from '@ionic-native/camera';
import {Storage} from "@ionic/storage";
import {CustomerProvider} from "../../providers/customer/customer";
import {Ilocation} from "../../interfaces/location.interface";
import {IDelivery} from "../../interfaces/delivery.interface";
import {ICustomer} from "../../interfaces/customer.interface";
import {DeliveriesPage} from "../deliveries/deliveries";
import {DeliveryDetailsPage} from "../delivery-details/delivery-details";
import {isUndefined} from "ionic-angular/util/util";
import {RequestsPage} from "../requests/requests";
import {ContactsPage} from "../contacts/contacts";
import {ImagePicker} from "@ionic-native/image-picker";
import {PageGmapAutocomplete} from "../page-gmap-autocomplete/page-gmap-autocomplete";

/**
 * Generated class for the NewDeliveryPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare let Bugsnag;
@IonicPage()
@Component({
    selector: 'page-new-delivery',
    templateUrl: 'new-delivery.html',
})
export class NewDeliveryPage {

    pickUpName: string;
    pickUpAddress: string;
    pickUpLat: number;
    pickUpLng: number;
    dropOffName: string;
    dropOffAddress: string;
    dropOffLat: number;
    dropOffLng: number;
    images: string[] = [];
    stepOne: boolean = true;
    stepTwo: boolean = false;
    firstname: string;
    surname: string;
    phone: string;
    email: string;
    price: number;
    description:string;
    paymentMethod: string = "cash";
    loading: Loading;
    customer: ICustomer;
    interval: any;
    deliveryItems: any[];
    itemType: any;
    itemCategory: any[];
    ditid: number;
    vcid:number;
    busyNotification:boolean = true;

    //errors
    firstnameError:string;
    phoneError:string;
    categoryError:string;
    itemError:string;
    dropOffError:string;
    pickUpError:string;

    //others
    deliveryType:string;
    error:string;
    message:string;
    messageDelay:number = 5000;
    showPhoto: boolean = true;


    constructor(public navCtrl: NavController,
                private camera: Camera,
                public customerProvider: CustomerProvider,
                public storage: Storage,
                public platform: Platform,
                public alertCtrl: AlertController,
                private imagePicker: ImagePicker,
                public actionSheetCtrl: ActionSheetController,
                public loadingCtrl: LoadingController,
                public navParams: NavParams) {


        this.customer = this.customerProvider.customer;

        //get receiver details
        this.storage.get('receiverFname').then((value) => {
            this.firstname = value;
        });
        this.storage.get('receiverSname').then((value) => {
            this.surname = value;
        });
        this.storage.get('receiverPhone').then((value) => {
            this.phone = value;
        });
        this.storage.get('receiverEmail').then((value) => {
            this.email = value;
        });

        this.getDeliveryItems();

        if (this.navParams.get('type') == 'PickUp') {

            this.pickUpName = this.navParams.get('name');
            this.pickUpAddress = this.navParams.get('address');
            this.pickUpLng = this.navParams.get('lng');
            this.pickUpLat = this.navParams.get('lat');

            this.storage.set('pickUpName', this.pickUpName);
            this.storage.set('pickUpAddress', this.pickUpAddress);
            this.storage.set('pickUpLng', this.pickUpLng);
            this.storage.set('pickUpLat', this.pickUpLat);

            this.storage.get('dropOffName').then((value) => {
                this.dropOffName = value;
            });

            this.storage.get('dropOffAddress').then((value) => {
                this.dropOffAddress = value;
            });

            this.storage.get('dropOffLng').then((value) => {
                this.dropOffLng = value;
            });

            this.storage.get('dropOffLat').then((value) => {
                this.dropOffLat = value;
                this.getPrice();
            });

            this.storage.get('deliveryType').then((value) => {
                this.deliveryType = value;
            });


        } else if (this.navParams.get('type') == 'DropOff' ) {

            this.dropOffName = this.navParams.get('name');
            this.dropOffAddress = this.navParams.get('address');
            this.dropOffLng = this.navParams.get('lng');
            this.dropOffLat = this.navParams.get('lat');
            this.storage.set('dropOffName', this.dropOffName);
            this.storage.set('dropOffAddress', this.dropOffAddress);
            this.storage.set('dropOffLng', this.dropOffLng);
            this.storage.set('dropOffLat', this.dropOffLat);


            this.storage.get('pickUpName').then((value) => {
                this.pickUpName = value;
            });

            this.storage.get('pickUpAddress').then((value) => {
                this.pickUpAddress = value;
            });

            this.storage.get('pickUpLng').then((value) => {
                this.pickUpLng = value;
            });

            this.storage.get('pickUpLat').then((value) => {
                this.pickUpLat = value;
                this.getPrice();
            });

            this.storage.get('deliveryType').then((value) => {
                this.deliveryType = value;
            });
        }

        if(this.navParams.get('type') == 'Contact'){
            this.storage.get('pickUpName').then((value) => {
                this.pickUpName = value;
            });

            this.storage.get('pickUpAddress').then((value) => {
                this.pickUpAddress = value;
            });

            this.storage.get('pickUpLng').then((value) => {
                this.pickUpLng = value;
            });

            this.storage.get('pickUpLat').then((value) => {
                this.pickUpLat = value;
                this.getPrice();
            });

            this.storage.get('deliveryType').then((value) => {
                this.deliveryType = value;

            });

            this.storage.get('dropOffName').then((value) => {
                this.dropOffName = value;
            });

            this.storage.get('dropOffAddress').then((value) => {
                this.dropOffAddress = value;
            });

            this.storage.get('dropOffLng').then((value) => {
                this.dropOffLng = value;
            });

            this.storage.get('dropOffLat').then((value) => {
                this.dropOffLat = value;
                this.getPrice();
            });

            this.storage.get('deliveryType').then((value) => {
                this.deliveryType = value;
            });

        }


        if(!isUndefined(this.navParams.get('request'))){

            let thisRequest = this.navParams.get('request');

            console.log(thisRequest.firstname);
            this.firstname = thisRequest.firstname;
            this.surname = thisRequest.surname;
            this.phone = thisRequest.phone;
            this.email = thisRequest.email;
            this.pickUpName = thisRequest.pickUpName;
            this.pickUpAddress = thisRequest.pickUpAddress;
            this.pickUpLat = thisRequest.pickUpLat;
            this.pickUpLng = thisRequest.pickUpLng;
            this.dropOffName = thisRequest.dropOffName;
            this.dropOffAddress = thisRequest.dropOffAddress;
            this.dropOffLat = thisRequest.dropOffLat;
            this.dropOffLng = thisRequest.dropOffLng;
            this.price = thisRequest.price;
            this.paymentMethod = thisRequest.paymentMethod;
            this.customer.cid = thisRequest.cid;
            this.itemType = thisRequest.itemType;
            this.deliveryType = thisRequest.deliveryType;
            this.vcid = thisRequest.vcid;

            this.getPrice();
            console.log(this.navParams.get('request'));

        }

        this.storage.get('vcid').then((value) => {
            if(isUndefined(value) || value === null) this.vcid = 1;
            else this.vcid = value;
            console.log('vcid is ' + value);
        });

    }


    ionViewDidLoad() {
        if(this.platform.is('browser')){
            this.showPhoto = false;
        }
    }


    handleError(error) {
        console.log(error);
    }

    deleteImage(i) {
        this.images.splice(i, 1);
    }

    getPrice() {

        this.price = null;
        let fromLocation: Ilocation = new Ilocation();
        fromLocation.lat = this.pickUpLat;
        fromLocation.lng = this.pickUpLng;

        let toLocation: Ilocation = new Ilocation();
        toLocation.lat = this.dropOffLat;
        toLocation.lng = this.dropOffLng;

        this.customerProvider.getPrice(fromLocation, toLocation,this.vcid).subscribe((success) => {
            this.price = success;
        }, (error) => {
            this.handleError(error);
        });


    }

    addContact(){

        this.storage.set('pickUpName', this.pickUpName);
        this.storage.set('pickUpAddress', this.pickUpAddress);
        this.storage.set('pickUpLng', this.pickUpLng);
        this.storage.set('pickUpLat', this.pickUpLat);

        this.storage.set('dropOffName', this.dropOffName);
        this.storage.set('dropOffAddress', this.dropOffAddress);
        this.storage.set('dropOffLng', this.dropOffLng);
        this.storage.set('dropOffLat', this.dropOffLat);

        this.storage.set('vcid', this.vcid);
        this.storage.set('deliveryType',this.deliveryType).then(success=>console.log(success));
        this.navCtrl.push(ContactsPage);
    }

    getLocation(type) {

        this.navCtrl.push( PageGmapAutocomplete, {'type': type});
    }

    getDeliveryItems() {
        console.log('Getting delivery Items');
        this.storage.get('deliveryItems').then((value)=>{
           this.deliveryItems = value;
        });
        this.customerProvider.deliveryItems().subscribe((success) => {
            this.deliveryItems = success;
            this.storage.set('deliveryItems',success);
        }, (error) => {
            console.log(error)
        });
    }

    nextStep() {


        this.firstnameError = null; // clear values
        this.phoneError = null; // clear values

        // handle delivery Type
        if(NewDeliveryPage.isEmpty(this.deliveryType)) {

            this.setError("Please select a type of delivery");
            return;
        }

        this.storage.set('deliveryType',this.deliveryType).then(success=>console.log(success));
        // end delivery type handling


        if(this.deliveryType != "receiver"){
            if(!NewDeliveryPage.isValidString(this.firstname) ) {
                this.firstnameError = "*required";
            } else if( !NewDeliveryPage.isValidNumber(this.phone) ){
                this.phoneError = "*required";
            } else {
                this.storage.set('receiverFname', this.firstname);
                this.storage.set('receiverSname', this.surname);
                this.storage.set('receiverPhone', this.phone);
                this.storage.set('receiverEmail', this.email);
                this.storage.set('vcid', this.vcid);
                this.stepOne = false;
                this.stepTwo = true;
            }

        } else {
            this.stepOne = false;
            this.stepTwo = true;
        }

    }

    previousStep() {
        this.stepOne = true;
        this.stepTwo = false;
    }

    takePhoto() {

        const options: CameraOptions = {
            quality: 100,
            saveToPhotoAlbum: false,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            targetWidth: 800,
            targetHeight: 800
        };

        this.camera.getPicture(options).then((imagePath) => {

            // let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
            // let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);

            this.images.push(imagePath);

        }, (error) => {
            console.log(error)
            // Handle error
        });
    }


    uploadPhoto(){

        let options = {
            maximumImagesCount: 10,
            width: 800,
            height: 800,
            quality: 100
        };

        this.imagePicker.getPictures(options).then((results) => {

            for (var i = 0; i < results.length; i++) {

                this.images.push(results[i]);
            }

        }, (err) => {
            this.showAlert("Error","Failed to select photo. Please try again.");
        });
    }

    photoAction() {

        let actionSheet = this.actionSheetCtrl.create({
            title: 'Please confirm your action',
            buttons: [
                {
                    text: 'Take Photo',
                    handler: () => {
                        this.takePhoto();
                    }
                },{

                    text: 'Upload Photo',
                    handler: () => {
                        this.uploadPhoto();
                    }
                },{
                    text: 'Close Menu',
                    role: 'cancel',
                    handler: () => {
                        //
                    }
                }
            ]
        });
        actionSheet.present();
    }



    request() {

        this.itemError = null; //clear existing values
        this.categoryError = null; //clear existing values
        this.dropOffError = null; //clear existing values
        this.pickUpError = null; //clear existing values


        if(this.pickUpLat == null || this.pickUpLng == null ||
            this.pickUpName == null  || this.pickUpAddress == null) {
            this.setError("Please select a pick-up location");
            this.pickUpError = "*required";
            return;
        }

        if(this.dropOffLat == null || this.dropOffLng == null ||
            this.dropOffName == null  || this.dropOffAddress == null) {
            this.setError("Please select a drop-off location");
            this.dropOffError = "*required";
            return;
        }

        if(this.itemCategory == null ){
            this.setError("Please select the item category");
            this.categoryError = "*required";
            return;
        }

        if(this.itemType == null ) {
            this.setError("Please select the item type");
            this.itemError = "*required";
            return;
        }



        let delivery = new IDelivery();
        delivery.fname = this.firstname;
        delivery.sname = this.surname;
        delivery.phone = this.phone;
        delivery.email = this.email;
        delivery.images = this.images;
        delivery.from = this.pickUpName;
        delivery.fromAddress = this.pickUpAddress;
        delivery.fromLat = this.pickUpLat;
        delivery.fromLng = this.pickUpLng;
        delivery.to = this.dropOffName;
        delivery.toAddress = this.dropOffAddress;
        delivery.toLat = this.dropOffLat;
        delivery.toLng = this.dropOffLng;
        delivery.amount = this.price;
        delivery.paymentMethod = this.paymentMethod;
        delivery.cid = this.customer.cid;
        delivery.ditid = this.itemType;
        delivery.description = this.description;
        delivery.deliveryType = this.deliveryType;
        delivery.vcid = this.vcid;

        let loading = this.loadingCtrl.create({
            content: 'Finding a dropper for you. Please wait...'
        });


        this.storage.get('requests').then((success:IDelivery[])=>{
            success.push(delivery);
            this.storage.set('requests', success);
        });

        this.loading = loading;
        loading.present();



        let self = this;
        this.customerProvider.newDelivery(delivery).then((value) => {
            this.storage.remove('receiverFname');
            this.storage.remove('receiverSname');
            this.storage.remove('receiverPhone');
            this.storage.remove('receiverEmail');


                    setTimeout(() => {

                        if (this.busyNotification) {
                            let alert = this.alertCtrl.create({
                                title: 'New Delivery',
                                subTitle: 'Your delivery request has been received. Our droppers are busy but one of our staff has been alerted' +
                                ' you would be assigned a dropper real soon!',
                                buttons: ['OK']
                            });
                            alert.present();
                            this.loading.dismissAll();
                            this.navCtrl.setRoot(DeliveriesPage);
                        }

                    }, 180000);

                    setTimeout(function () {
                        self.isDeliveryAccepted();
                    }, 10000);

        }, (error) => {

            this.showAlert("Error",error);
            this.loading.dismissAll();

        });

        // clear storage after request
        this.storage.remove('dropOffName');
        this.storage.remove('dropOffAddress');
        this.storage.remove('dropOffLng');
        this.storage.remove('dropOffLat');
        this.storage.remove('pickUpName');
        this.storage.remove('pickUpAddress');
        this.storage.remove('pickUpLng');
        this.storage.remove('pickUpLat');
        this.storage.remove('receiverFname');
        this.storage.remove('receiverSname');
        this.storage.remove('receiverPhone');
        this.storage.remove('receiverEmail');


    }

    isDeliveryAccepted() {
//        console.log('checking if delivery is accepted');

        let self = this;
        try {
            console.log(this.customerProvider.isFinishedRequesting);
            if (this.customerProvider.isFinishedRequesting) {

                console.log('has finished requesting');
                this.customerProvider.deliveryInProgress(0).subscribe((delivery) => { // 0 would make it use last did

                    if (delivery == 0) {
//                        console.log('not accepted yet');
                        setTimeout(function () {
                            self.isDeliveryAccepted();
                        }, 15000);
                    } else {
                        this.busyNotification = false;
                        self.loading.dismissAll();
                        self.navCtrl.setRoot(DeliveryDetailsPage, {'delivery': delivery});
                    }
                }, (error) => {
//                    console.log(error);
                });

            } else {
//                console.log('Still Uploading Images');
                setTimeout(function () {
                    self.isDeliveryAccepted();
                }, 15000);
            }

        } catch (e) {
            setTimeout(function () {
                self.isDeliveryAccepted();
            }, 3000);
        }

    }

    addToFavorites(){
        let delivery = new IDelivery();
        delivery.fname = this.firstname;
        delivery.sname = this.surname;
        delivery.phone = this.phone;
        delivery.email = this.email;
        delivery.images = this.images;
        delivery.from = this.pickUpName;
        delivery.fromAddress = this.pickUpAddress;
        delivery.fromLat = this.pickUpLat;
        delivery.fromLng = this.pickUpLng;
        delivery.to = this.dropOffName;
        delivery.toAddress = this.dropOffAddress;
        delivery.toLat = this.dropOffLat;
        delivery.toLng = this.dropOffLng;
        delivery.amount = this.price;
        delivery.paymentMethod = this.paymentMethod;
        delivery.cid = this.customer.cid;
        delivery.ditid = this.itemType;
        delivery.deliveryType = this.deliveryType;
        delivery.vcid = 1;

        this.storage.get('requests').then((success:IDelivery[])=>{

            if(success === null){
                success = [];
            } else {
                success.push(delivery);
            }
            this.storage.set('requests', success).then((success)=>{
                this.showAlert("Successful", "Request added to favourites");
            });
        });

    }

    showRequests(){
        this.navCtrl.push(RequestsPage);
    }

    showAlert(title,message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

    setError(message){
        this.message = null;
        this.error = message;
        setTimeout(()=>{this.error = null;},this.messageDelay);
    }

    setMessage(message){
        this.error = null;
        this.message = message;
        setTimeout(()=>{this.message = null;},this.messageDelay);

    }

    categorySelected(item) {

        this.itemCategory = item.items;
        console.log(this.itemCategory);
    }

    static isEmpty(data){
        return (data === null || data === "" || isUndefined(data));
    }
    static isValidString(data){
        let  regexp = new RegExp('[a-z]+');
        let    test = regexp.test(data);
        return !(data == null || data == "" || !test);
    }

    static isValidNumber(data){
        let  regexp = new RegExp('[0-9]+');
        let    test = regexp.test(data);
        return !(data == null || data == "" || !test);
    }

}
