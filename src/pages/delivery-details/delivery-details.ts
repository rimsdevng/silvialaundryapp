import {Component, ElementRef, ViewChild} from '@angular/core';
import {ActionSheetController, AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {CustomerProvider} from "../../providers/customer/customer";
import {isUndefined} from "ionic-angular/util/util";
import {SupportPage} from "../support/support";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {SMS} from "@ionic-native/sms";


declare let google;
@IonicPage()
@Component({
  selector: 'page-delivery-details',
  templateUrl: 'delivery-details.html',
})
export class DeliveryDetailsPage {
    @ViewChild('map') mapElement: ElementRef;
    map: any;
    delivery;
    isInProgress:boolean = false;
    showCompletionButton:boolean = false;
    showIssueButton:boolean = false;
    interval:any;
    marker:any;
    infoWindow:any;
    clicked:boolean = false;
    url:string;

    constructor(public navCtrl: NavController,
                public customerProvider: CustomerProvider,
                public alertCtrl: AlertController,
                private iab: InAppBrowser,
                private sms: SMS,
                public actionSheetCtrl: ActionSheetController,
                public navParams: NavParams) {

        this.delivery =  this.navParams.get('delivery');
        if(this.delivery.status == "Pending" || this.delivery.status == "Available" ){
            this.isInProgress = true;
        }

    }

    ionViewDidLoad() {

        console.log('ionViewDidLoad Delivery Details');

        if(this.delivery.status == "Pending" && this.delivery.droppedOffAt !== null && this.delivery.pickedUpAt !== null) {
            console.log('in the function');
            this.showCompletionButton = true;
            this.showIssueButton = true;
        }

        if(this.delivery.status == "Issue" && this.delivery.droppedOffAt !== null && this.delivery.pickedUpAt !== null) {
            console.log('in the function');
            this.showCompletionButton = true;
            this.showIssueButton = false;
        }


        if(this.isInProgress){
            this.loadMap();
            setTimeout(()=>{
                this.updateDropperLocation();
            },500)

        }
    }

    completeDelivery(delivery){
        this.clicked = true;
        this.customerProvider.deliveryCompleted(delivery.did).subscribe((success)=>{
            console.log(success);
            if(success == 1) this.showCompletionButton = false;
            if(success == 1) this.showIssueButton = false;
            this.clicked = false;
        },(error)=>{
            this.clicked = false;
            console.log(error);
        });
    }

    issueDelivery(delivery){
        this.clicked = true;
        this.customerProvider.deliveryIssue(delivery.did).subscribe((success)=>{
            console.log(success);
            if(success == 1) this.showCompletionButton = true;
            if(success == 1) this.showIssueButton = false;
            this.clicked = false;
        },(error)=>{
            console.log(error);
            this.clicked = false;
        });

    }

    support(){
        this.navCtrl.push(SupportPage);
    }

    loadMap(){


        if(this.delivery.dropper.latitude > 0 || this.delivery.dropper.longitude > 0){
            let latLng = new google.maps.LatLng( this.delivery.dropper.latitude , this.delivery.dropper.longitude);

            let mapOptions = {
                center: latLng,
                zoom: 18,
                tilt:30,
                disableDefaultUI:false,
                mapTypeControl:false,
                zoomControl:false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                clickableIcons:false
            };

            this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

            this.infoWindow = new google.maps.InfoWindow();

            this.infoWindow.setContent('Your dropper is here!');


        } else
        {
            let latLng = new google.maps.LatLng(9.070400, 7.487462);

            let mapOptions = {
                center: latLng,
                zoom: 18,
                tilt:30,
                disableDefaultUI:false,
                mapTypeControl:false,
                zoomControl:false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                clickableIcons:false
            };

            this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
        }

       this.interval =  setInterval(()=>{

           this.updateDropperLocation();
        },15000);

    }

    updateDropperLocation(){
        console.log("Getting dropper location");
        this.customerProvider.getDropperLocation(this.delivery.dropper.drid).subscribe((success)=>{

            // if(!isUndefined(this.marker))
                // this.marker.setMap(null); // clear the existing marker
            let myLatLng = new google.maps.LatLng(success.lat, success.lng);

            console.log(success);

            console.log(myLatLng);

            let image = {
                url: 'http://dropster.gurudeveloperinc.com/img/dropbike.png',
                // This marker is 20 pixels wide by 32 pixels high.
                size: new google.maps.Size(32, 20),
                // The origin for this image is (0, 0).
                origin: new google.maps.Point(0, 0),
                // The anchor for this image is the base of the flagpole at (0, 32).
                anchor: new google.maps.Point(20, 0),
                scaledSize : new google.maps.Size(32, 20)
            };

            console.log(image);

            if(isUndefined(this.marker)){
                this.marker = new google.maps.Marker({
                    position: myLatLng,
                    title: 'Your dropper is here!',
                    icon: image
                });
                this.marker.setMap(this.map);

            }



            // this.infoWindow.open(this.map, this.marker);
            this.marker.setPosition(myLatLng);
            // this.infoWindow.open(this.map);
            this.map.setCenter(myLatLng);
        },(error)=>{ console.log(error)});

    }


    cancelDelivery(delivery){
        console.log('delivery cancel clicked.');
        this.clicked = true;

        let data = {
            'did' : delivery.did
        };

        this.customerProvider.cancelDelivery(data).subscribe((success)=>{
            this.clicked = false;
            if(success == 1){
                this.showAlert('Success','Your delivery has been cancelled.');
                this.navCtrl.pop();
            }

            if(success == 2 ){
                this.showAlert('Success','Your delivery has been already been cancelled.');
                this.navCtrl.pop()
            }

            if(success == 0){
                this.showAlert('Error',"An error occurred while trying to cancel the delivery request. Please try again or contact support.");
            }

        },(error)=>{
            this.clicked = false;
            this.showAlert('Error',"An error occurred while trying to cancel the delivery request. Please try again or contact support.");
        });

    }


    presentConfirmation(delivery) {

        let actionSheet = this.actionSheetCtrl.create({
            title: 'Please confirm your action',
            buttons: [
                {
                    text: 'Cancel Delivery Request',
                    role: 'destructive',
                    handler: () => {
                        this.cancelDelivery(delivery);
                    }
                },{
                    text: 'Close Menu',
                    role: 'cancel',
                    handler: () => {
                        //
                    }
                }
            ]
        });
        actionSheet.present();
    }


    phoneAction(phone){
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Please confirm your action',
            buttons: [
                {
                    text: 'Call',
                    handler: () => {

                        this.url = "tel: " + phone;
                        this.navigate();

                    }
                },{
                    text: 'SMS',
                    handler: () => {
                        //CONFIGURATION
                        let options = {
                            replaceLineBreaks: false, // true to replace \n by a new line, false by default
                            android: {
                                intent: 'INTENT'  // send SMS with the native android SMS messaging
                            }
                        };

                        this.sms.send( phone , '',options);
                    }
                },{
                    text: 'Whats App',
                    handler: () => {
                        this.url = "https://api.whatsapp.com/send?phone=" + phone;
                        this.navigate();
                    }
                },{
                    text: 'Close Menu',
                    role: 'cancel',
                    handler: () => {
                        //
                    }
                }
            ]
        });
        actionSheet.present();

    }


    ionViewWillLeave(){
        console.log('clearing interval');
        clearInterval(this.interval);
    }

    showAlert(title,message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }


    navigate(){
        let browser = this.iab.create(this.url,"_system","location=no,hidden=yes");

    }


}
