import { Component } from '@angular/core';
import {IonicPage, LoadingController, NavController, NavParams} from 'ionic-angular';

/**
 * Generated class for the SupportPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-support',
  templateUrl: 'support.html',
})
export class SupportPage {
    loading:any;
  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public navParams: NavParams) {
      this.presentLoadingScreen();

  }

  ionViewDidLoad() {
      console.log('ionViewDidLoad SupportPage');
  }


    presentLoadingScreen() {
        let loading = this.loadingCtrl.create({
            content: "Contacting our CEO. You're that special. Please wait..."
        });

        this.loading = loading;
        loading.present();
    }

    dismissLoadingScreen(){
      this.loading.dismiss();
    }


}
