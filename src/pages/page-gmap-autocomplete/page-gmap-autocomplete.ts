import { Component} from '@angular/core';
import {NavController, ModalController, NavParams} from 'ionic-angular';
import { ModalAutocompleteItems } from '../modal-autocomplete-items/modal-autocomplete-items';
import {NewDeliveryPage} from "../new-delivery/new-delivery";
import {Geolocation} from "@ionic-native/geolocation";

declare let google:any;

@Component({
    selector: 'page-page-gmap-autocomplete',
    templateUrl: 'page-gmap-autocomplete.html'
})
export class PageGmapAutocomplete {

    address:any = {
        place: '',
        set: false,
    };
    placesService:any;
    map: any;
    markers = [];
    placedetails: any;

    constructor(public navCtrl: NavController,
                public modalCtrl: ModalController,
                public geolocation: Geolocation,
                public navParams: NavParams) {
    }

    ionViewWillEnter() {
        this.initMap();
        this.initPlacedetails();
        this.getCurrentLocation();
    }


    getCurrentLocation(){

        this.geolocation.getCurrentPosition().then((resp) => {
            this.placedetails.lat = resp.coords.latitude;
            this.placedetails.lng = resp.coords.longitude;
            this.placedetails.name = "Your current location";
            this.placedetails.address = "Customer's Location";

            let infowindow = new google.maps.InfoWindow();
            let infowindowContent = document.getElementById('infowindow-content');
            infowindowContent.children['place-name'].textContent = "Your current location";


            infowindow.setContent(infowindowContent);
            let latLng = new google.maps.LatLng(this.placedetails.lat, this.placedetails.lng);

            let marker = new google.maps.Marker({
                map: this.map,
                position: latLng
            });
            this.markers.push(marker);


            infowindow.open(this.map, marker);

            infowindow.setPosition(latLng);
            infowindow.open(this.map);
            this.map.setCenter(latLng);


        }).catch((error) => {
            console.log('Error getting location', error);
        });

    }


    showModal() {
        // reset 
        this.reset();
        // show modal|
        let modal = this.modalCtrl.create(ModalAutocompleteItems);
        modal.onDidDismiss(data => {
            //console.log('page > modal dismissed > data > ', data);
            if(data){
                this.address.place = data.description;
                // get details
                this.getPlaceDetail(data.place_id);
            }                
        });
        modal.present();
    }

    private reset() {
        this.initPlacedetails();
        this.address.place = '';
        this.address.set = false;
    }

    private getPlaceDetail(place_id:string):void {
        let self = this;
        let request = {
            placeId: place_id
        };
        this.placesService = new google.maps.places.PlacesService(this.map);
        this.placesService.getDetails(request, callback);
        function callback(place, status) {
            if (status == google.maps.places.PlacesServiceStatus.OK) {
           //     console.log('page > getPlaceDetail > place > ', place);
                // set full address
                self.placedetails.name =place.name;
                self.placedetails.address = place.formatted_address;
                self.placedetails.icon  = place.icon;
                self.placedetails.lat = place.geometry.location.lat();
                self.placedetails.lng = place.geometry.location.lng();
                for (let i = 0; i < place.address_components.length; i++) {
                    let addressType = place.address_components[i].types[0];
                    let values = {
                        short_name: place.address_components[i]['short_name'],
                        long_name: place.address_components[i]['long_name']
                    }
                    if(self.placedetails.components[addressType]) {
                        self.placedetails.components[addressType].set = true;
                        self.placedetails.components[addressType].short = place.address_components[i]['short_name'];
                        self.placedetails.components[addressType].long = place.address_components[i]['long_name'];
                    }                                     
                }                  
                // set place in map
                self.map.setCenter(place.geometry.location);
                self.createMapMarker(place);
                // populate
                self.address.set = true;
                //console.log('page > getPlaceDetail > details > ', self.placedetails);
            }else{
                //console.log('page > getPlaceDetail > status > ', status);
            }
        }
    }

    private initMap() {
        let latLng = new google.maps.LatLng(9.070400, 7.487462);
        let divMap = (<HTMLInputElement>document.getElementById('map'));
        this.map = new google.maps.Map(divMap, {
            center: latLng,
            zoom: 15,
            disableDefaultUI: true,
            draggable: false,
            zoomControl: true
        });
    }

    private createMapMarker(place:any):void {

        let infowindow = new google.maps.InfoWindow();
        let infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);

        let placeLoc = place.geometry.location;
        let marker = new google.maps.Marker({
          map: this.map,
          position: placeLoc
        });    
        this.markers.push(marker);

        infowindowContent.children['place-icon'].src = this.placedetails.icon;
        infowindowContent.children['place-name'].textContent = this.placedetails.name;
        infowindowContent.children['place-address'].textContent = this.placedetails.address;
        infowindow.setPosition(placeLoc);
        infowindow.open(this.map);
        this.map.setCenter(placeLoc);

    }

    private initPlacedetails() {
        this.placedetails = {
            address: '',
            lat: '',
            lng: '',
            name: '',
            icon: '',
            components: {
                route: { set: false, short:'', long:'' },                           // calle 
                street_number: { set: false, short:'', long:'' },                   // numero
                sublocality_level_1: { set: false, short:'', long:'' },             // barrio
                locality: { set: false, short:'', long:'' },                        // localidad, ciudad
                administrative_area_level_2: { set: false, short:'', long:'' },     // zona/comuna/partido 
                administrative_area_level_1: { set: false, short:'', long:'' },     // estado/provincia 
                country: { set: false, short:'', long:'' },                         // pais
                postal_code: { set: false, short:'', long:'' },                     // codigo postal
                postal_code_suffix: { set: false, short:'', long:'' },              // codigo postal - sufijo
            }    
        };        
    }

    confirm(){

        this.navCtrl.setRoot(NewDeliveryPage,{
            type: this.navParams.get('type'),
            name:this.placedetails.name,
            address:this.placedetails.address,
            lat:this.placedetails.lat,
            lng:this.placedetails.lng
        });
    }

}
