import {Component, ElementRef, ViewChild} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import {
    GoogleMaps,
    GoogleMap,
    GoogleMapsEvent,
    LatLng,
    CameraPosition,
    MarkerOptions,
    Marker
} from '@ionic-native/google-maps';
import {NewDeliveryPage} from "../new-delivery/new-delivery";


/**
 * Generated class for the MapPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

declare let google;
@IonicPage()
@Component({
  selector: 'page-map',
  templateUrl: 'map.html',
})
export class MapPage {

    @ViewChild('map') mapElement: ElementRef;
    map: any;
    lat:number;
    lng:number;
    address:string;
    name:string;

  constructor(public navCtrl: NavController,
              private geolocation: Geolocation,
              private androidPermissions: AndroidPermissions,
              public navParams: NavParams) {
  }



  ionViewDidLoad() {

      this.loadMap();
      this.getCurrentLocation();
    console.log('ionViewDidLoad MapPage');
  }

  back(){
      this.navCtrl.pop();
  }

  getCurrentLocation(){

      this.geolocation.getCurrentPosition().then((resp) => {
          this.lat = resp.coords.latitude;
          this.lng = resp.coords.longitude;
          let self = this;

          let latLng = new google.maps.LatLng(this.lat, this.lng);

          let mapOptions = {
              center: latLng,
              zoom: 17,
              tilt:30,
              disableDefaultUI:false,
              mapTypeControl:false,
              zoomControl:false,
              mapTypeId: google.maps.MapTypeId.ROADMAP
          };

          this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);


          let infoWindow = new google.maps.InfoWindow();

          self.name = "Your current location";
          self.address = "Customer's Location";


          infoWindow.setContent(document.getElementById('currentLocationContent') );

          let marker = new google.maps.Marker({
              map: this.map,
              anchorPoint: new google.maps.Point(0, -29)
          });

          infoWindow.open(this.map, marker);

          infoWindow.setPosition(latLng);
          infoWindow.open(this.map);
          this.map.setCenter(latLng);



      }).catch((error) => {
          console.log('Error getting location', error);
      });

  }

    loadMap(){

        let latLng = new google.maps.LatLng(9.070400, 7.487462);

        let mapOptions = {
            center: latLng,
            zoom: 15,
            tilt:30,
            disableDefaultUI:false,
            mapTypeControl:false,
            zoomControl:false,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            clickableIcons:false
        };

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);


    }

    checkBounds(){

        let strictBounds = new google.maps.LatLngBounds(
            new google.maps.LatLng(28.70, -127.50),
            new google.maps.LatLng(48.85, -55.90)
        );

        // Listen for the dragend event
        google.maps.event.addListener(this.map, 'dragend', function() {
            if (strictBounds.contains(this.map.getCenter())) return;

            // We're out of bounds - Move the map back within the bounds

            let c = this.map.getCenter(),
                x = c.lng(),
                y = c.lat(),
                maxX = strictBounds.getNorthEast().lng(),
                maxY = strictBounds.getNorthEast().lat(),
                minX = strictBounds.getSouthWest().lng(),
                minY = strictBounds.getSouthWest().lat();

            if (x < minX) x = minX;
            if (x > maxX) x = maxX;
            if (y < minY) y = minY;
            if (y > maxY) y = maxY;

            this.map.setCenter(new google.maps.LatLng(y, x));
        });
    }
    autoComplete(){

        let input = document.getElementById('input').getElementsByTagName('input')[0];

        let str = input.value;
        let prefix = 'Abuja, ';
        if(str.indexOf(prefix) == 0) {
            console.log(input.value);
        } else {
            if (prefix.indexOf(str) >= 0) {
                input.value = prefix;
            } else {
                input.value = prefix+str;
            }
        }

        let options = {
            language: 'en-US',
            // types: ['(cities)'],
            componentRestrictions: { country: "ng" }
        };

        let autocomplete = new google.maps.places.Autocomplete(input,options);
        autocomplete.bindTo('bounds', this.map);

        // to make variables accessible from within the autocomplete
        //add listener method
        let map = this.map;
        let self = this;



        let infowindow = new google.maps.InfoWindow();
        let infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        let marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });





        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            let place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.

                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17);  // Why 17? Because it looks good.
            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);

            let address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || ''),
                    (place.address_components[3] && place.address_components[3].short_name || '')
                ].join(' ');
            }


            self.name = place.name;
            self.address = address;
            self.lat = place.geometry.viewport.f.f;
            self.lng = place.geometry.viewport.b.b;


            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            // infowindowContent.children['place-lat'].textContent = "Lat: " + place.geometry.viewport.f.f;
            // infowindowContent.children['place-lng'].textContent = "Lng: " + place.geometry.viewport.b.b;

            infowindow.open(map, marker);
        });


    }

    confirm(){

        console.log(this.name);
        console.log(this.address);
        this.navCtrl.setRoot(NewDeliveryPage,{
            type: this.navParams.get('type'),
            name:this.name,
            address:this.address,
            lat:this.lat,
            lng:this.lng
        });
    }


}
