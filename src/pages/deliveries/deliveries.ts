import { Component } from '@angular/core';
import {ActionSheetController, AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {CustomerProvider} from "../../providers/customer/customer";
import {DeliveryDetailsPage} from "../delivery-details/delivery-details";
import {isUndefined} from "ionic-angular/util/util";

@IonicPage()
@Component({
  selector: 'page-deliveries',
  templateUrl: 'deliveries.html',
})
export class DeliveriesPage {

    type:string = "outgoing";
    outgoingDeliveries:any[];
    incomingDeliveries:any[];
    loading:boolean = true;


    constructor(public navCtrl: NavController,
              public alertCtrl: AlertController,
              public customerProvider: CustomerProvider,
              public actionSheetCtrl: ActionSheetController,
              public navParams: NavParams) {

      if(!isUndefined(this.navParams.get('type')))
      this.type = this.navParams.get('type');

    }

    ionViewDidEnter(){
      this.outgoing();
      this.incoming();
    }

    ionViewDidLoad() {
      this.outgoing();
      this.incoming();
    }

    details(item){


      if(item.status == "Available"){
          let alert = this.alertCtrl.create({
              title: 'Delivery Status',

              message: 'Your delivery request has not been accepted yet. Once it is, you would be notified and be able ' +
              'to see the details here!',
              buttons: [
                  {
                      text: 'Okay',
                      role: 'cancel',

                      handler: () => {

                      }
                  },
                  {
                      text:'Cancel Delivery',
                      role:'destructive',
                      handler: () => {
                          this.presentConfirmation(item);
                      }
                  }
              ]

          });
          alert.present();

      } else
     this.navCtrl.push(DeliveryDetailsPage,{'delivery':item});

    }

    outgoing(){
      this.loading = true;
      this.customerProvider.sentDeliveries().subscribe((value)=>{
          this.outgoingDeliveries = value;
          this.loading = false;
          console.log(value);
      },(error)=>{
          console.log(error);
      });
    }

    incoming(){
      this.loading = true;
      this.customerProvider.receivedDeliveries().subscribe((value)=>{
          this.incomingDeliveries = value;
          this.loading = false;
          console.log(value);
      },(error)=>{
          console.log(error);
      });
    }

    cancelDelivery(delivery){
        let data = {
            'did' : delivery.did
        };
        console.log('delivery cancel clicked.');

        this.showAlert("Loading","Cancellation in progress");

        this.customerProvider.cancelDelivery(data).subscribe((success)=>{
            if(success == 1){
                this.showAlert('Success','Your delivery with txn ID' + delivery.did +' has been cancelled.');
            }

            if(success == 2 ){
                this.showAlert('Success','Your delivery with txn ID ' + delivery.did +' has been already been cancelled.');
            }

            if(success == 0){
                this.showAlert('Error',"An error occurred while trying to cancel the delivery request. Please try again or contact support.");
            }

        },(error)=>{
            this.showAlert('Error',"An error occurred while trying to cancel the delivery request. Please try again or contact support.");
        });

    }

    presentConfirmation(delivery) {

        let actionSheet = this.actionSheetCtrl.create({
            title: 'Please confirm your action',
            buttons: [
                {
                    text: 'Cancel Delivery Request',
                    role: 'destructive',
                    handler: () => {
                        this.navCtrl.pop();
                        this.cancelDelivery(delivery);
                    }
                },{
                    text: 'Close Menu',
                    role: 'cancel',
                    handler: () => {
                        //
                    }
                }
            ]
        });
        actionSheet.present();
    }


    showAlert(title,message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }


}
