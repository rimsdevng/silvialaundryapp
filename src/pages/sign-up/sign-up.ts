import {Component} from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {CustomerProvider} from "../../providers/customer/customer";
import {ICustomer} from "../../interfaces/customer.interface";
import {HomePage} from "../home/home";
import {SupportPage} from "../support/support";
import {Validation} from "../../helpers/validation";
import {Storage} from "@ionic/storage";
import {DashboardPage} from "../dashboard/dashboard";
import {SigninPage} from "../signinemail/signinemail";

@IonicPage()
@Component({
    selector: 'page-sign-up',
    templateUrl: 'sign-up.html',
})
export class SignUpPage {

    fname: string;
    sname: string;
    password: string;
    confirmPassword: string;
    phone: string;
    username: string;
    gender: string = 'Male';
    email: string;
    loading: boolean = false;
    referredBy: string;
    message: string;
    error: string;
    toast: any;
    currentTip:string;
    currentFocus:string;


    //errors

    fnameError: string;
    snameError: string;
    passwordError: string;
    phoneError: string;
    usernameError: string;
    emailError: string;

    //tips
    usernameTip: string = "This should be any unique nickname.";
    fnameTip: string = "Your real name ;)";
    snameTip: string = "Your real surname please :)";
    passwordTip: string = "Make it tough! You can recover it if you forget it.";
    phoneTip: string = "Please add country code for non-Nigerian numbers eg. 08081234567 or +2348081234567" ;
    emailTip: string = "Please use an email you have access to";
    referralTip: string = "Please ensure you type the right code. Cross-check";


    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public storage: Storage,
                public  alertCtrl: AlertController,
                public customerProvider: CustomerProvider,
                ) {
    }


    ionViewWillEnter(){

        this.storage.get('signupFname').then((success)=>{this.fname = success;});
        this.storage.get('signupSname').then((success)=>{this.sname = success;});
        this.storage.get('signupEmail').then((success)=>{this.email = success;});
        this.storage.get('signupUsername').then((success)=>{this.username = success;});
        this.storage.get('signupGender').then((success)=>{if(success !== null) this.gender = success;});
        this.storage.get('signupPhone').then((success)=>{this.phone = success;});
        this.storage.get('signupReferral').then((success)=>{this.referredBy = success;});
    }

    signIn() {
        this.navCtrl.setRoot(HomePage, {});
    }

    signUp() {

        this.fnameError = null;
        this.snameError = null;
        this.passwordError = null;
        this.phoneError = null;
        this.usernameError = null;
        this.emailError = null;

        if(Validation.isEmpty(this.username) ){
            this.setError("Please enter your username");
            this.usernameError = "*required";
            return;
        }

        if(!Validation.isValidString(this.fname) ){
            this.setError("Please enter your first name");
            this.fnameError = "*required";
            return;
        }

        if(!Validation.isValidString(this.sname) ){
            this.setError("Please enter your surname");
            this.snameError = "*required";
            return;
        }

        if(!Validation.isValidEmail(this.email) ){
            this.setError("Please enter a valid email");
            this.emailError = "*required";
            return;
        }

        if(!Validation.isValidNumber(this.phone) ){
            this.setError("Please enter your phone number");
            this.phoneError = "*required";
            return;
        }

        this.loading = true;

        if (this.fname == null || this.sname == null || this.phone == null) {
            this.passwordError = "Please fill all required fields";
            this.loading = false;
         }

        if (this.password != this.confirmPassword){
            this.setError("Passwords don't match. Please retype");
         }

        if (this.password == this.confirmPassword) {

            this.passwordError = null;

            let customer = new ICustomer();

            customer.fname = this.fname;
            customer.sname = this.sname;
            customer.email = this.email;
            customer.username = this.username;
            customer.password = this.password;
            customer.gender = this.gender;
            customer.phone = this.phone;



            this.customerProvider.signUp(customer).subscribe((success) => {

                console.log(success);

                if(success == 1) {

                    this.storage.remove('signupFname');
                    this.storage.remove('signupSname');
                    this.storage.remove('signupEmail');
                    this.storage.remove('signupEmail');
                    this.storage.remove('signupGender');
                    this.storage.remove('signupPhone');
                    this.storage.remove('signupReferral');


                    let customer = new ICustomer();
                    customer.username = this.email;
                    customer.password = this.password;

                    this.customerProvider.signIn(customer).subscribe((success) => {
                        console.log(success);
                        if (success == 0) {

                            this.setError("Sorry wrong credentials. Try again.");
                            this.loading = false;
                            this.navCtrl.setRoot(SigninPage);
                        } else {

                            this.storage.set('customer', success);
                            this.storage.set('isSignedIn', true);

                            this.customerProvider.customer = success;

                            if (this.customerProvider.customer.role == "Dropper") {
                                this.customerProvider.showDropperMenu = true;
                                console.log("This is a dropper so we would track");
                                this.customerProvider.startTracking();
                            } else {
                                this.customerProvider.showDropperMenu = false;
                            }

                            this.customerProvider.showVpMenu = this.customerProvider.customer.role == "Vehicle Partner";

                            this.navCtrl.setRoot(DashboardPage);
                        }


                    }, (error) => {
                        this.showAlert("Status", "Couldn't sign you in automatically. Please sign in");
                        this.navCtrl.setRoot(SigninPage);
                    });


                    this.showAlert("Status", 'Successfully Signed Up. Signing you in...');
                } else {
                    this.loading = false;
                    this.showAlert("Status",success.message);
                }


            }, (error) => {
                this.handleError(error);
            });

        }
    }

    support() {
        this.navCtrl.push(SupportPage);
    }

    setError(error){
        this.error = error;
        this.loading = false;
        let self = this;
        this.showAlert('Error',error);
        setTimeout(function () {
            self.error = null;
        },5000);
    }

    showAlert(title,message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

    handleError(error) {
        this.loading = false;
        console.log(error);
    }

    showToast(currentFocus,message){

        this.storage.set('signupFname',this.fname);
        this.storage.set('signupSname',this.sname);
        this.storage.set('signupEmail',this.email);
        this.storage.set('signupUsername',this.username);
        this.storage.set('signupGender',this.gender);
        this.storage.set('signupPhone',this.phone);
        this.storage.set('signupReferral',this.referredBy);


        this.currentFocus = currentFocus;
        this.currentTip = message;

    }

    blur(){
        this.currentTip = null;
        this.currentFocus = null;
    }

}
