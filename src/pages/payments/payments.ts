import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import { Storage } from "@ionic/storage";
import {ICustomer} from "../../interfaces/customer.interface";
import {CustomerProvider} from "../../providers/customer/customer";
/**
 * Generated class for the PaymentsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
declare let PaystackPop;
@IonicPage()
@Component({
  selector: 'page-payments',
  templateUrl: 'payments.html',
})
export class PaymentsPage {

    amount:number;
    paymentMethod:string = 'card';
    customer:ICustomer;
    couponCode:string;
    displayAddFunds:boolean;
    displayPayments:boolean = true;
    payments:any[];
    clicked:boolean = false;

    error:string;
    message:string;


    constructor(public navCtrl: NavController,
                public storage: Storage,
                public  alertCtrl: AlertController,
                public customerProvider:CustomerProvider,
                public navParams: NavParams) {

        this.customer  = this.customerProvider.customer;

        }

    ionViewDidLoad() {
        console.log('ionViewDidLoad PaymentsPage');
    }

    ionViewDidEnter(){
        this.customerPayment();
    }

    addFunds(){
        this.clicked = true;
        if(this.paymentMethod == 'card'){
            this.cardPayment();
        }

        if(this.paymentMethod == 'coupon'){
            this.couponPayment();
        }

    }

    toggleAddFunds(){
        this.displayAddFunds = !this.displayAddFunds;
        this.displayPayments = !this.displayPayments;
    }

    couponPayment(){
         let data = {
            'cid': this.customer.cid,
            'couponCode' : this.couponCode
        };
         let self = this;

        this.customerProvider.addCoupon(data).subscribe((success)=>{
            this.clicked = false;
            console.log(success);

            if(success == 1){
                this.customerProvider.updateCustomer();
                this.showAlert("Successful","Coupon Added Successfully");
                self.toggleAddFunds();
                self.customerPayment();

            } else {
                this.setError(success.message);
            }


        },(error)=>{
            console.log(error);
            this.clicked = false;
        });
    }


    cardPayment(){
        let self = this;
        PaystackPop.setup({
            key: 'pk_live_120a4ff5ce930997baffe9e36f5910bc0d86bbc2',
            email: self.customer.email,
            amount: self.amount * 100,
            container: 'paystackEmbedContainer',
            callback: function(response){

                self.customerProvider.updateCustomer();
                self.customerPayment();
                self.toggleAddFunds();

            },
        });

    }

    customerPayment(){
        this.customerProvider.customerPayments(this.customer.cid).subscribe((success)=>{
            this.payments = success.reverse();
        },(error)=>{console.log(error)});
    }


    setError(error){
        this.error = error;
        this.clicked = false;
        let self = this;
        this.showAlert('Error',error);
        setTimeout(function () {
            self.error = null;
        },5000);
    }

    showAlert(title,message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }



}
