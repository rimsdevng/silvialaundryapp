import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CustomerProvider} from "../../providers/customer/customer";
import {Storage} from "@ionic/storage";
import {ICustomer} from "../../interfaces/customer.interface";

/**
 * Generated class for the EarningsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-earnings',
  templateUrl: 'earnings.html',
})
export class EarningsPage {
    customer:ICustomer;
    vehicles:any[];
    total:number = 0;

  constructor(public navCtrl: NavController,
              public customerProvider: CustomerProvider,
              public storage: Storage,
              public navParams: NavParams) {


      this.storage.get('customer').then((success)=>{
          this.customer = success;

          this.customerProvider.earnings(this.customer.cid).subscribe((success)=>{
              this.vehicles = success.vehicles;
              console.log(success.vehicles);
              for( let i=0; i < this.vehicles.length; i++){
                  this.total += this.vehicles[i].earnings;
              }
          },(error)=>{console.log(error)});

      });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EarningsPage');
  }

}
