import { Component } from '@angular/core';
import {ActionSheetController, AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {Storage} from "@ionic/storage";
import {ICustomer} from "../../interfaces/customer.interface";
import {CustomerProvider} from "../../providers/customer/customer";
import {Camera, CameraOptions} from '@ionic-native/camera';
import {FileTransfer, FileTransferObject} from "@ionic-native/file-transfer";
import {ImagePicker} from "@ionic-native/image-picker";
import {InAppBrowser} from "@ionic-native/in-app-browser";

/**
 * Generated class for the ProfilePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {
    customer:ICustomer;
    showProfile:boolean = true;
    editing:boolean = false;
    editingAccount:boolean = false;
    showPasswordChange = false;
    username:string;
    bankCode:string;
    fname:string;
    sname:string;
    email:string;
    phone:string;
    message:string;
    error:string;
    messageDelay:number = 5000;
    accountName:string;
    accountNumber:number;
    bankName:string;
    password:string;
    newPassword:string;
    confirmPassword:string;
    url:string;
    loaded:boolean = false;
    loading:boolean = false;
    photoLoading:boolean = false;


    constructor(public navCtrl: NavController,
                public storage:Storage,
                private camera: Camera,
                public alertCtrl: AlertController,
                private transfer: FileTransfer,
                private iab: InAppBrowser,
                private imagePicker: ImagePicker,
                public actionSheetCtrl: ActionSheetController,
                public customerProvider:CustomerProvider,
                public navParams: NavParams) {


        this.customer = this.customerProvider.customer;


    }

    ionViewDidLoad() {
        console.log('ionViewDidLoad ProfilePage');
        this.customerProvider.updateCustomer();

    }

    editProfile(){
        this.fname = this.customerProvider.customer.fname;
        this.sname = this.customerProvider.customer.sname;
        this.email = this.customerProvider.customer.email;
        this.username = this.customerProvider.customer.username;
        this.phone = this.customerProvider.customer.phone;
        this.showProfile = false;
        this.editing = !this.editing;
    }


    takePhoto() {

        this.photoLoading = true;

        const options: CameraOptions = {
            quality: 100,
            saveToPhotoAlbum: false,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            correctOrientation: true,
            targetWidth: 800,
            targetHeight: 800
        };

        this.camera.getPicture(options).then((imagePath) => {


//            let currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
//            let correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);


            let option:any = {
                fileKey: "image",
                fileName: "image.jpg",
                chunkedMode: false,
                mimeType: "multipart/form-data",
                params : {cid: this.customerProvider.customer.cid},
            };

            const fileTransfer: FileTransferObject = this.transfer.create();


            let self = this;

            fileTransfer.upload(imagePath , this.customerProvider.url + 'api/change-profile-photo', option).then(data => {

                console.log(data);
                self.showAlert("Successful","Your profile image has been updated.");

                self.photoLoading = false;

            }, err => {
                console.log(err);

                self.photoLoading = false;
            });


        }, (error) => {
            console.log(error);
            this.photoLoading = false;
            // Handle error
        });
    }

    uploadPhoto(){

        let options = {
            maximumImagesCount: 1,
            width: 800,
            height: 800,
            quality: 100
        };

        this.imagePicker.getPictures(options).then((results) => {

            this.photoLoading = true;
            for (var i = 0; i < results.length; i++) {

                let option:any = {
                    fileKey: "image",
                    fileName: "image.jpg",
                    chunkedMode: false,
                    mimeType: "multipart/form-data",
                    params : {cid: this.customerProvider.customer.cid},
                };

                const fileTransfer: FileTransferObject = this.transfer.create();


                let self = this;

                fileTransfer.upload(results[i] , this.customerProvider.url + 'api/change-profile-photo', option).then(data => {

                    self.showAlert("Successful","Your profile image has been updated.");

                    self.photoLoading = false;

                }, err => {
                    console.log(err);

                    self.photoLoading = false;
                });

            }
        }, (err) => {
            this.showAlert("Error","Failed to select photo. Please try again.");
        });
    }

    photoAction() {

        let actionSheet = this.actionSheetCtrl.create({
            title: 'Please confirm your action',
            buttons: [
                {
                    text: 'Take Photo',
                    handler: () => {
                        this.takePhoto();
                    }
                },{

                    text: 'Upload Photo',
                    handler: () => {
                        this.uploadPhoto();
                    }
                },{
                    text: 'Close Menu',
                    role: 'cancel',
                    handler: () => {
                        //
                    }
                }
            ]
        });
        actionSheet.present();
    }




    updateProfile(){
        this.loading = true;

        let customer = new ICustomer();
        customer.cid = this.customer.cid;
        customer.fname = this.fname;
        customer.sname = this.sname;
        customer.email = this.email;
        customer.username = this.username;
        customer.phone = this.phone;

        this.customerProvider.updateProfile(customer).subscribe((success)=>{

          this.loading = false;
          if(success == 1) {
              this.customerProvider.updateCustomer();
              this.editing = false;
              this.showProfile = true;
              this.setMessage("Successfully Updated");
          } else {

              this.setError('An error occurred. Please try again');
          }
        },(error)=>{
            this.setError('An error occurred. Please try again');
        });

    }

    editAccountDetails(){

        this.editingAccount = !this.editingAccount;
        this.showProfile = false;
    }

    updateBankDetails(){

        this.loading = true;
        switch (this.bankCode){
            case  "044":
                this.bankName = "Access Bank";
                break;

            case  "023":
                this.bankName = "Citibank Nigeria";
                break;

            case  "063":
                this.bankName = "Diamond Bank";
                break;

            case  "050":
                this.bankName = "Ecobank Nigeria";
                break;

            case  "084":
                this.bankName = "Enterprise Bank";
                break;

            case  "070":
                this.bankName = "Fidelity Bank";
                break;

            case  "011":
                this.bankName = "First Bank of Nigeria";
                break;

            case  "214":
                this.bankName = "First City Monument Bank";
                break;

            case  "058":
                this.bankName = "Guaranty Trust Bank";
                break;

            case  "030":
                this.bankName = "Heritage Bank";
                break;

            case  "082":
                this.bankName = "Keystone Bank";
                break;

            case  "014":
                this.bankName = "MainStreet Bank";
                break;

            case  "076":
                this.bankName = "Skye Bank";
                break;

            case  "221":
                this.bankName = "Stanbic IBTC Bank";
                break;

            case  "068":
                this.bankName = "Standard Chartered Bank";
                break;

            case  "232":
                this.bankName = "Sterling Bank";
                break;

            case  "032":
                this.bankName = "Union Bank of Nigeria";
                break;

            case  "033":
                this.bankName = "United Bank For Africa";
                break;

            case  "215":
                this.bankName = "Unity Bank";
                break;

            case  "035":
                this.bankName = "Wema Bank";
                break;

            case  "057":
                this.bankName = "Zenith Bank";
                break;


            default:
        }

        let data = {
            'cid' : this.customerProvider.customer.cid,
            'accountName' : this.accountName,
            'accountNumber' : this.accountNumber,
            'accountBank' : this.bankName,
            'bankCode' : this.bankCode
        };

        this.customerProvider.updateBankDetails(data).subscribe((success)=>{
            this.loading = false;
            if(success == 1){
                this.customerProvider.updateCustomer();
                this.editingAccount = false;
                this.showProfile = true;
                this.setMessage("Successfully Updated.");
            } else {
                this.setError("An error occurred. Please try again");
            }
        },(error)=>{console.log(error)});
    }

    getBankAccountName(){

        this.customerProvider.resolveBankAccountname(this.bankCode,this.accountNumber).subscribe((success)=>{
            this.accountName = success.data.account_name;
        },(error)=>{console.log(error)});
    }

    changePassword(){
        this.showPasswordChange = true;
        this.showProfile = false;
    }

    updatePassword(){
        if(this.newPassword == this.confirmPassword ){
            let data = {
                'email' : this.customerProvider.customer.email,
                'password' : this.password,
                'newPassword' : this.newPassword
            };

            let self = this;

            this.customerProvider.changePassword(data).subscribe((success)=>{

                if(success == 1){
                    self.showPasswordChange =  false;
                    self.showProfile = true;
                    self.setMessage("Password Updated Successfully");
                } else {
                    self.setError("Current password is wrong. Please try again");
                }
            },(error)=>{console.log(error)});

        } else {
            this.setError("New passwords don't match. Please try again");
        }
    }

    back(){
        this.showProfile = true;
        this.editing = false;
        this.editingAccount = false;
        this.showPasswordChange = false;
    }


    setError(message){
        this.message = null;
        this.error = message;
        this.loading = false;
        setTimeout(()=>{this.error = null;},this.messageDelay);
    }

    setMessage(message){
        this.error = null;
        this.message = message;
        setTimeout(()=>{this.message = null;},this.messageDelay);

    }

    showAlert(title,message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

    shareCode(){
        this.url = "https://api.whatsapp.com/send?text=Hey!%20Sign%20Up%20for%20dropster%20at%20www.dropsterng.com/app%20with%20my%20referral%20code%20and%20you%20would%20get%20a%20bonus.Thank%20me%20later.%20%20Ref Code%20-%20" + this.customerProvider.customer.referralCode;
        this.navigate();

    }

    navigate(){

        this.iab.create(this.url,"_system","location=no,hidden=yes");

    }


}
