import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import {SignUpPage} from "../sign-up/sign-up";
import {CustomerProvider} from "../../providers/customer/customer";
import {ICustomer} from "../../interfaces/customer.interface";
import {DashboardPage} from "../dashboard/dashboard";
import {Storage} from "@ionic/storage";
import {HomePage} from "../home/home";
import {Validation} from "../../helpers/validation";
import {SupportPage} from "../support/support";

@IonicPage()
@Component({
  selector: 'page-signinemail',
  templateUrl: 'signinemail.html'
})
export class SigninPage {
    username:string;
    password:string;
    email:string;
    showSignIn:boolean = true;
    showforgotPassword:boolean = false;
    showVerifyCode:boolean = false;
    code:string;
    message:string;
    newPassword:string;
    confirmNewPassword:string;
    public error:string;
    public clicked:boolean = false;
    messageDelay:number = 3000;
    rememberMe:boolean = false;

    constructor(public navCtrl: NavController,
              public customerProvider: CustomerProvider,
              public storage:Storage
              ) {

    }



	signUp(){
        this.navCtrl.setRoot(SignUpPage, {});
	}


	signIn(){

	    this.error = null;
        this.clicked = true;

        if(this.username == null || this.password == null ){
            this.error = "Please fill all fields";
            this.clicked = false;
        } else {
            let customer = new ICustomer();
            customer.username = this.username;
            customer.password = this.password;

            this.customerProvider.signIn(customer).subscribe((success)=>{
                console.log(success);
                if(success == 0 ){

                    this.error = "Sorry wrong credentials. Try again.";
                    this.clicked = false;
                } else {


                    this.storage.set('customer',success);
                    this.storage.set('isSignedIn',true);

                    this.customerProvider.customer = success;

                    if(this.customerProvider.customer.role == "Dropper"){
                        this.customerProvider.showDropperMenu = true;
                        console.log("This is a dropper so we would track");
                        this.customerProvider.startTracking();
                    } else {
                        this.customerProvider.showDropperMenu = false;
                    }

                    this.customerProvider.showVpMenu = this.customerProvider.customer.role == "Vehicle Partner";

                    this.navCtrl.setRoot(DashboardPage);
                }


            },(error)=>{this.handleError(error)});

        }

    }

    buttonPressed(keyCode){
	    if(keyCode == 13){
	        this.signIn();
        }
    }


    goToHome(){
        this.navCtrl.setRoot(HomePage);
    }

    forgotPasswordEP(keyCode){
        if(keyCode == 13){
            this.forgotPassword();
        }
    }

    verifyCodeEP(keyCode){
        if(keyCode == 13){
            this.verifyCode();
        }
    }

    forgotPassword(){
	    this.error = null;
	    this.showforgotPassword = true;
	    this.showSignIn = false;
	    let data = {'email' : this.email};

	    console.log(this.email);

        if(!Validation.isValidEmail(this.email)) {
            this.setError("Please enter a valid email.");
            return;
        } else {

            this.clicked = true;
            this.customerProvider.forgotPassword(data).subscribe((success)=>{
                if(success == 1){
                    this.showforgotPassword = false;
                    this.showVerifyCode = true;
                    this.message = "Verification code sent to your email. Please type the code";
                }
                this.clicked = false;

            },(error)=>{
                this.setError("An error occurred. Please verify you typed the right email");
                console.log(error); this.clicked = false;});

        }
    }

    support(){
        this.navCtrl.push(SupportPage);
    }

    verifyCode(){
        if(this.newPassword == this.confirmNewPassword){
            this.clicked = true;
            let data = {
                'email' : this.email,
                'newPassword' : this.newPassword,
                'code'  : this.code
            };

            this.customerProvider.confirmVerification(data).subscribe((success)=>{
                this.clicked = false;
                if(success == 1){
                    this.setMessage("Password Reset Successful. Please Sign In.");
                    this.showVerifyCode = false;
                    this.showSignIn = true;


                } else {
                    this.setError("Invalid verification code. Please try again.");
                }
            },(error)=>{console.log(error)});

        } else {
            this.setError("Both passwords don't match. Please check");
        }
    }

    cancel(){
        this.showforgotPassword = false;
        this.showSignIn = true;

    }

    setError(message){
        this.message = null;
        this.error = message;
        setTimeout(()=>{this.error = null;},this.messageDelay);
    }

    setMessage(message){
        this.error = null;
        this.message = message;
        setTimeout(()=>{this.message = null;},this.messageDelay);

    }

    handleError(error){
      console.log(error);

        this.error = "Couldn't connect to internet. Try again.";
        this.clicked = false;
    }

}





