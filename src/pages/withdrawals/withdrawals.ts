import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CustomerProvider} from "../../providers/customer/customer";


@IonicPage()
@Component({
  selector: 'page-withdrawals',
  templateUrl: 'withdrawals.html',
})
export class WithdrawalsPage {
    withdrawals:any[];

    constructor(public navCtrl: NavController,
                public customerProvider:CustomerProvider,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        this.customerProvider.withdrawals(this.customerProvider.customer.cid).subscribe((success)=>{
            this.withdrawals = success.withdrawals.reverse();
        },(error)=>{console.log(error)});
        console.log('ionViewDidLoad WithdrawalsPage');
    }

}
