import { Component } from '@angular/core';
import {IonicPage, NavController} from 'ionic-angular';
import { SigninPage }    from '../signinemail/signinemail';
import {SignUpPage} from "../sign-up/sign-up";
import {Storage} from "@ionic/storage";
import {DashboardPage} from "../dashboard/dashboard";
import {Facebook, FacebookLoginResponse} from "@ionic-native/facebook";
import {CustomerProvider} from "../../providers/customer/customer";
import {ICustomer} from "../../interfaces/customer.interface";

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
    private showPhone:boolean = false;
    private showCode:boolean = false;
    private phone:string;
    private loading:boolean = false;
    verifyText:string = "Submit";
    code:string;
    error:string;
    message:string;
    messageDelay:number = 5000;
    customer = new ICustomer();

    constructor(public navCtrl: NavController,
              private fb:Facebook,
              public customerProvider:CustomerProvider,
              public storage: Storage
              ) {

          this.storage.get('isSignedIn').then(value => {
              console.log(value);
              if(value) this.navCtrl.setRoot(DashboardPage);
          });

    }



	email(item){

        this.navCtrl.push(SigninPage, {

        });
    }


  	signUp(){
        this.navCtrl.push(SignUpPage, {

        });
  	}


    facebookSignIn(){
        this.fb.login(['public_profile', 'email'])
            .then((res: FacebookLoginResponse) =>
            {

                this.customerProvider.getFacebookUser(res.authResponse.userID,res.authResponse.accessToken).subscribe((success)=>{

                    let fbdata = success;

                    let data = {'email' : success.email, 'type': 'facebook'};
                    this.customerProvider.socialSignIn(data).subscribe((success)=>{
                        if(success == 0){
                            this.customer.fname = fbdata.first_name;
                            this.customer.sname = fbdata.last_name;
                            this.customer.email = fbdata.email;
                            this.customer.username = fbdata.first_name + fbdata.last_name;
                            this.customer.password = "facebook";
                            this.customer.image = fbdata.picture.data.url;
                            this.customer.gender = fbdata.gender;

                            this.showPhone = true;

                        } else {

                            this.storage.set('isSignedIn',true);
                            this.storage.set('customer',success);
                            this.customerProvider.customer = success;
                            this.navCtrl.setRoot(DashboardPage);
                        }
                    },(error)=>{console.log(error)});

                    console.log(success);
                },(error)=>{console.log(error)});

            })
            .catch(e => console.log('Error logging into Facebook', e));
    }


    verifyPhone(){

        this.loading = true;
        this.verifyText = "Submitting";

        let data = {
            'phone' : this.phone,
        };

        this.customerProvider.sendPhoneVerification(data).subscribe((success)=>{
            if(success == 1){

                this.customer.phone = this.phone;

                this.setMessage("We sent you a code. Please check your phone.");

                this.loading = false;
                this.verifyText = "Submit";

                this.showPhone = false;
                this.showCode = true;

            } else {
                this.setError("An error occurred while adding your number. Please try again.");
                this.loading = false;
                this.verifyText = "Submit";
            }

        },(error)=>{
            this.setError("An error occurred while adding your number. Please try again.");
            this.loading = false;
            this.verifyText = "Submit";
            console.log(error)
        });

    }

    confirmPhone(){

        this.loading = true;
        this.verifyText = "Submitting";
        let data = {
            'phone' : this.phone,
            'code'  : this.code
        };

        this.customerProvider.confirmPhoneVerification(data).subscribe((success)=>{

            if(success == 1){
                this.verifyText = "Signing in";
                this.setMessage("Phone Verified. Signing you in.");
                this.customerProvider.socialSignUp(this.customer).subscribe((success)=>{

                    if(success == 1 ){
                        let data = {'email' : this.customer.email, 'type': 'facebook'};
                        this.customerProvider.socialSignIn(data).subscribe((success)=>{
                            console.log(success);
                            this.customerProvider.customer = success;
                            this.storage.set('isSignedIn',true);
                            this.storage.set('customer',success);
                            this.navCtrl.setRoot(DashboardPage)
                        },(error)=>{console.log(error)});

                    } else {
                        this.setError("An error occurred at sign in. Please try again.");
                        this.loading = false;
                        this.verifyText = "Submit";

                    }
                },(error)=>{

                    this.setError("An error occurred at sign in. Please try again.");
                    this.loading = false;
                    this.verifyText = "Submit";
                    console.log(error)
                });

            } else {
                this.setError("Invalid Verification Code.");
                this.loading = false;
                this.verifyText = "Submit";

            }

        },(error)=>{console.log(error)});

    }

    setError(message){
        this.message = null;
        this.error = message;
        setTimeout(()=>{this.error = null;},this.messageDelay);
    }

    setMessage(message){
        this.error = null;
        this.message = message;
        setTimeout(()=>{this.message = null;},this.messageDelay);

    }


}
