import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CustomerProvider} from "../../providers/customer/customer";

/**
 * Generated class for the AffiliatePage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-affiliate',
  templateUrl: 'affiliate.html',
})
export class AffiliatePage {
    referred:any[];

    constructor(public navCtrl: NavController,
                public customerProvider:CustomerProvider,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        this.customerProvider.referred(this.customerProvider.customer.cid).subscribe((success)=>{
            this.referred = success;
        },(error)=>{console.log(error)});
        //console.log('ionViewDidLoad AffiliatePage');
    }



}
