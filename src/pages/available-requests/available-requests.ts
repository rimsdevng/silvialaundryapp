import { Component } from '@angular/core';
import {AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {IDelivery} from "../../interfaces/delivery.interface";
import {CustomerProvider} from "../../providers/customer/customer";
import {InProgressPage} from "../in-progress/in-progress";
import {ICustomer} from "../../interfaces/customer.interface";
import {Storage} from "@ionic/storage";
import {DashboardPage} from "../dashboard/dashboard";

/**
 * Generated class for the AvailableRequestsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-available-requests',
  templateUrl: 'available-requests.html',
})
export class AvailableRequestsPage {
    availableDeliveries:IDelivery[];
    customer:ICustomer;
    loading:boolean = false;
    loadingPage:boolean = true;
    itemLoading:any;

    constructor(public navCtrl: NavController,
              public storage: Storage,
              public alertCtrl: AlertController,
              public customerProvider: CustomerProvider,
              public navParams: NavParams) {


      this.storage.get('customer').then((value)=>{
          this.customer = value;
          //console.log(value);
          if(this.customer.dropper.vid === null){
              this.showAlert("Not Permitted", "You have no vehicle assigned to you please contact a staff.");
              this.navCtrl.setRoot(DashboardPage);
          } else {
              this.update();
          }

      });


    }

    ionViewDidLoad() {
    //console.log('ionViewDidLoad AvailableRequestsPage');

    }

    showAlert(title,message) {
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();
    }

    update(){
        this.customerProvider.availableDeliveries().subscribe((value)=>{
          this.availableDeliveries = value;
          this.loadingPage = false;

        },(error)=>{
            let self = this;
            setTimeout(function(){
                self.update();
            },3000);
            //console.log('retrying the update');
        });
    }

    acceptDelivery(item:IDelivery){

         //console.log("Accepting");
         this.loading = true;
         this.itemLoading = item;
         let dropper = {'did': item.did, 'cid': this.customer.cid };
         let self = this;

         this.customerProvider.acceptDelivery(dropper).subscribe((value)=>{

         this.customerProvider.deliveryInProgress(item.did).subscribe((value) => {

             self.navCtrl.setRoot(InProgressPage,{'delivery':value});
         }, (error) => {
             this.loading = false;
             console.log(error);
             this.acceptDelivery(item);
             console.log('Retrying');
         });


         },(error)=>{
             this.showAlert("Error","Accepting failed.<br>Please ensure you have internet and try again. If it continues, contact support.");
             this.loading = false;
              console.log(error);
         });
    }

}
