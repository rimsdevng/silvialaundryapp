import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AvailableRequestsPage } from './available-requests';

@NgModule({
  declarations: [
    AvailableRequestsPage,
  ],
  imports: [
    IonicPageModule.forChild(AvailableRequestsPage),
  ],
  exports: [
    AvailableRequestsPage
  ]
})
export class AvailableRequestsPageModule {}
