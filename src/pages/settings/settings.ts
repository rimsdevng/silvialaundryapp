import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CustomerProvider} from "../../providers/customer/customer";

/**
 * Generated class for the SettingsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
    dailyPayouts:boolean;
    message:string;
    error:string;
    messageDelay:number = 5000;

    constructor(public navCtrl: NavController,
                public customerProvider:CustomerProvider,
                public navParams: NavParams) {

        this.updateSettings();
    }

    ionViewDidLoad() {

    }


    updateSettings(){
        this.customerProvider.settings().subscribe((success)=>{
            console.log('settings updated');
            this.dailyPayouts = success.dailyPayout;
        },(error)=>{console.log(error);setTimeout(()=>{this.updateSettings(),2000})});

    }

    changePayout(){
        console.log('toggled');
        console.log(this.dailyPayouts);
        let data = {
            'cid' : this.customerProvider.customer.cid,
            'dailyPayout' : this.dailyPayouts
        };
        this.customerProvider.changePayout(data).subscribe((success)=>{

            if(success == 1) this.setMessage("Payout schedule changed");
            else this.setError("An error occurred. Please try again");
            this.updateSettings();
        },(error)=>{console.log(error);});
    }

    setError(message){
        this.message = null;
        this.error = message;
        setTimeout(()=>{this.error = null;},this.messageDelay);
    }

    setMessage(message){
        this.error = null;
        this.message = message;
        setTimeout(()=>{this.message = null;},this.messageDelay);

    }


}
