import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from "@ionic/storage";
import {IDelivery} from "../../interfaces/delivery.interface";
import {NewDeliveryPage} from "../new-delivery/new-delivery";

/**
 * Generated class for the RequestsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-requests',
  templateUrl: 'requests.html',
})
export class RequestsPage {

    public requests:IDelivery[];

    constructor(public navCtrl: NavController,
                public storage: Storage,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {
        this.storage.get('requests').then((success)=>{
            this.requests = success;
        });

    }

    selectRequest(request){
        this.navCtrl.setRoot(NewDeliveryPage,{'request':request});

    }



}
