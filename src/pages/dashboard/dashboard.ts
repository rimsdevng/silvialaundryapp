import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {CustomerProvider} from "../../providers/customer/customer";
import {Storage} from '@ionic/storage';
import {HomePage} from "../home/home";
import {ProfilePage} from "../profile/profile";
import {SupportPage} from "../support/support";
import {DeliveriesPage} from "../deliveries/deliveries";
import {NewDeliveryPage} from "../new-delivery/new-delivery";
import {PaymentsPage} from "../payments/payments";
import {EarningsPage} from "../earnings/earnings";
import {WithdrawalsPage} from "../withdrawals/withdrawals";
import {AffiliatePage} from "../affiliate/affiliate";
import {AvailableRequestsPage} from "../available-requests/available-requests";
import {InProgressPage} from "../in-progress/in-progress";
import {SettingsPage} from "../settings/settings";

/**
 * Generated class for the DashboardPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({

  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {

  constructor(public navCtrl: NavController,
              public navParams: NavParams,
              public customerProvider:CustomerProvider,
              public storage:Storage,
             ) {

      //clear new delivery records

      this.storage.remove('pickUpName');
      this.storage.remove('pickUpAddress');
      this.storage.remove('pickUpLat');
      this.storage.remove('pickUpLng');
      this.storage.remove('DropOffName');
      this.storage.remove('DropOffAddress');
      this.storage.remove('DropOffLat');
      this.storage.remove('DropOffLng');

      this.storage.get('isSignedIn').then(value => {
          if(!value) this.navCtrl.push(HomePage);
      });

  }

  ionViewDidLoad() {

      this.customerProvider.updateCustomer();
  }

  support(){
      this.navCtrl.setRoot(SupportPage);
  }
  profilePage(){
      this.navCtrl.setRoot(ProfilePage);
  }

  paymentsPage(){
      this.navCtrl.setRoot(PaymentsPage);
  }

  newDeliveryPage(){
      this.navCtrl.setRoot(NewDeliveryPage);
  }

  earningsPage(){
      this.navCtrl.setRoot(EarningsPage);
  }

  withdrawalsPage(){
      this.navCtrl.setRoot(WithdrawalsPage);
  }

  deliveries(){
      this.navCtrl.setRoot(DeliveriesPage);
  }

  affiliatePage(){
      this.navCtrl.setRoot(AffiliatePage);
  }

  availableRequestsPage(){
      this.navCtrl.setRoot(AvailableRequestsPage);
  }

  inProgressPage(){
      this.navCtrl.setRoot(InProgressPage);
  }

  settingsPage(){
      this.navCtrl.setRoot(SettingsPage);
  }
}
