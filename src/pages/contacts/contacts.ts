import { Component } from '@angular/core';
import {ActionSheetController, AlertController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {CustomerProvider} from "../../providers/customer/customer";
import {NewDeliveryPage} from "../new-delivery/new-delivery";
import {Storage} from "@ionic/storage";
import {Validation} from "../../helpers/validation";

/**
 * Generated class for the ContactsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-contacts',
  templateUrl: 'contacts.html',
})
export class ContactsPage {

    public fname:string;
    public sname:string;
    public phone:string;
    public email:string;
    public add:boolean = false;
    public contacts:any[];
    public clicked:boolean = false;
    loading:boolean = true;
    isEdit:boolean = false;
    cfid:number;
    message:string;
    error:string;

    constructor(public navCtrl: NavController,
                public alertCtrl: AlertController,
                public actionSheetCtrl: ActionSheetController,
                public storage: Storage,
                public customerProvider: CustomerProvider,
                public navParams: NavParams) {
    }

    ionViewDidLoad() {

        this.loadContacts();

    }

    loadContacts(){
        this.customerProvider.getFavorites().subscribe((success)=>{
            this.contacts = success;
            this.loading = false;
            console.log(success);
        });

    }

    saveContact(){

        if(!Validation.isValidString(this.fname)) {
            this.setError("Please enter a valid number");
            return;
        }

        if(!Validation.isValidNumber(this.phone)) {
            this.setError("Please enter a valid number");
            return;
        }

        if(!Validation.isEmpty(this.email)){
            if(!Validation.isValidEmail(this.email)){
                this.setError("Please enter a valid email");
                return;
            }
        }

        if(!Validation.isEmpty(this.sname)){
            if(!Validation.isValidString(this.sname)){
                this.setError("Please enter a valid surname");
                return;
            }
        }

        this.clicked = true;
        let data = {
            'fname'  : this.fname,
            'sname'  : this.sname,
            'phone' : this.phone,
            'email' : this.email,
            'cid'   : this.customerProvider.customer.cid
        };

        console.log(data);
        this.customerProvider.addFavorite(data).subscribe((success)=>{
           console.log(success);
            if(success == 1){
                this.loadContacts();
                this.showAlert('Save Successful','Your contact has been saved.');
            } else {
                this.showAlert('Error','Sorry, an error occurred. Please try again.');
            }
            this.clicked = false;
        },(error)=>{
            console.log(error);
            this.clicked = false;});
    }

    editContact(){
        this.clicked = true;
        let data = {
            'cfid'   : this.cfid,
            'fname'  : this.fname,
            'sname'  : this.sname,
            'phone' : this.phone,
            'email' : this.email,
            'cid'   : this.customerProvider.customer.cid
        };

        this.customerProvider.editFavorite(data).subscribe((success)=>{
            console.log(success);
            if(success == 1){
                this.switch();
                this.isEdit = false;
                this.loadContacts();
                this.showAlert('Save Successful','Your contact has been saved.');
            } else {
                this.showAlert('Error','Sorry, an error occurred. Please try again.');
            }
            this.clicked = false;
        },(error)=>{
            console.log(error);
            this.clicked = false;
        });

    }

    selectContact(contact){

        this.storage.set('receiverFname', contact.fname);
        this.storage.set('receiverSname', contact.sname);
        this.storage.set('receiverPhone', contact.phone);
        this.storage.set('receiverEmail', contact.email);

        this.navCtrl.setRoot(NewDeliveryPage,{type:'Contact'});
    }

    presentActionSheet(contact) {
        let self = this;
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Modify your contacts',
            buttons: [
                {
                    text: 'Edit',
                    handler: () => {
                        self.fname = contact.fname;
                        self.sname = contact.sname;
                        self.email = contact.email;
                        self.phone = contact.phone;
                        self.cfid  = contact.cfid;
                        self.isEdit = true;
                        self.switch();
                    }
                },{
                    text: 'Delete',
                    role: 'destructive',
                    handler: () => {

                        this.add = false;
                        let data = {
                            'cfid'   : self.cfid,
                        };

                        this.customerProvider.deleteFavorite(data).subscribe((success)=>{
                            console.log(success);
                            if(success == 1){
                                this.loadContacts();
                                this.showAlert('Deleted','Your contact has been deleted.');
                            } else {
                                this.showAlert('Error','Sorry, an error occurred. Please try again.');
                            }

                        },(error)=>{
                            console.log(error);
                        });

                    }
                },{
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        console.log('Cancel clicked');
                    }
                }
            ]
        });
        actionSheet.present();
    }

    showAlert(title,message){
        let alert = this.alertCtrl.create({
            title: title,
            subTitle: message,
            buttons: ['OK']
        });
        alert.present();

    }

    switch(){
        this.add = !this.add;
    }

    setError(message){
        this.message = null;
        this.error = message;
        setTimeout(()=>{this.error = null;},5000);
    }


}
