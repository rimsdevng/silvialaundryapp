import {Component, ElementRef, ViewChild} from '@angular/core';
import {ActionSheetController, IonicPage, NavController, NavParams} from 'ionic-angular';
import {CustomerProvider} from "../../providers/customer/customer";
import {isUndefined} from "ionic-angular/util/util";
import {ICustomer} from "../../interfaces/customer.interface";
import { InAppBrowser } from '@ionic-native/in-app-browser';
import {SMS} from "@ionic-native/sms";

declare let google;
@IonicPage()
@Component({
  selector: 'page-in-progress',
  templateUrl: 'in-progress.html',
})
export class InProgressPage {

    @ViewChild('map') mapElement: ElementRef;
    map: any;
    deliveries:any[];
    delivery:any;
    customer:ICustomer;
    displayDetails:boolean = false;
    loading:boolean = true;
    paymentMethod:string = "cash";
    showPayment:boolean = true;
    showPickUp:boolean = true;
    showDropOff:boolean = false;
    showReturned:boolean = false;
    interval:any;
    clicked:boolean = false;


    marker:any;
    first:boolean = false;
    url:string;

    constructor(public navCtrl: NavController,
                public customerProvider: CustomerProvider,
                public actionSheetCtrl: ActionSheetController,
                private sms: SMS,
                private iab: InAppBrowser,
                public navParams: NavParams) {

        this.customer = this.customerProvider.customer;
    }

    back(){
        this.deliveries = null;
        this.loading = true;
        this.map = null;
        this.first = false;
        this.displayDetails = false;
        this.updateDeliveries();
    }


    updateDeliveries(){
        if( !isUndefined(this.navParams.get('delivery')) ){
            console.log('from navparams');
            this.loading = false;
            this.details(this.navParams.get('delivery')) ;

        } else {
            this.customerProvider.deliveriesInProgress().subscribe((success)=>{
                this.loading = false;
                this.deliveries = success.reverse();
                console.log(success);
            },(error)=>{
                let self = this;
                setTimeout(function () {
                    self.updateDeliveries();
                },3000);
                console.log(error);
            });
        }

    }

    ionViewWillEnter() {
        this.updateDeliveries();
    }


    details(item){
        this.delivery = item;
        this.displayDetails = true;


        //show returned if delivery has been dropped off and its a return request
        if(this.delivery.deliveryType == 'return' && this.delivery.droppedOffAt !== null && this.delivery.returnedAt === null ) this.showReturned = true;
        if(this.delivery.paymentStatus != 'unpaid') this.showPayment = false;
        // if(this.delivery.paymentStatus == 'paid') {
        //     this.showPickUp = true;
        // }

        if(this.delivery.from == "Your current location") this.delivery.from = "Customer's current location";
        if(this.delivery.paymentStatus == 'paid' && this.delivery.pickedUpAt !== null) {
            this.showDropOff = true;
        }
        if(this.delivery.pickedUpAt !== null ) this.showPickUp = false;
        if(this.delivery.droppedOffAt !== null ) this.showDropOff = false;
        if(this.delivery.deliveryType == 'return' && this.delivery.returnedAt !== null ) this.showReturned = false;

        setTimeout(this.loadMap(),500);

    }

    paymentCollected(item){

        this.clicked = true;
        let data = {'did' : item.did,'drid': item.dropper.drid,'paymentMethod' : this.paymentMethod};
        this.customerProvider.paymentCollected(data).subscribe((success)=>{
            console.log(success);

            if(success == 1){
                this.showPayment = false;
                this.showPickUp = true;
                this.showDropOff = false;
            }
            this.clicked= false;

        },(error)=>{
            console.log(error);
            this.clicked = false;
        });
    }

    pickedUpDelivery(item){

        this.clicked = true;
        this.first = false;
        this.customerProvider.pickedUpDelivery(item.did).subscribe((success)=>{
            console.log(success);
            if(success == 1) {
                this.showPickUp = false;
                this.showDropOff = true;
            }
            this.clicked = false;
        },(error)=>{
            this.clicked = false;
            console.log(error);
        });
    }

    droppedOffDelivery(item){
        this.clicked = true;
        this.customerProvider.droppedOffDelivery(item.did).subscribe((success)=>{
            console.log(success);
            if(success == 1) this.showDropOff = false;
            this.clicked = false;
        },(error)=>{
            this.clicked = false;
            console.log(error);
        });
    }


    returnedDelivery(item){
        this.clicked = true;
        this.customerProvider.returnedDelivery(item.did).subscribe((success)=>{
            console.log(success);
            if(success == 1) this.showReturned = false;
            this.clicked = false;
        },(error)=>{
            this.clicked = false;
            console.log(error);
        });
    }


    loadMap(){

        if(this.delivery.pickedUpAt === null){

            this.url =  " https://www.google.com/maps/dir/?api=1&origins="
                + this.customerProvider.lat + ',' + this.customerProvider.lng +
                "&destination=" + this.delivery.fromLat + "," + this.delivery.fromLng;
            let latLng = new google.maps.LatLng( this.delivery.fromLat , this.delivery.fromLng);

            let mapOptions = {
                center: latLng,
                zoom: 18,
                tilt:30,
                disableDefaultUI:false,
                mapTypeControl:false,
                zoomControl:false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                clickableIcons:false
            };

            this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

            this.marker = new google.maps.Marker({
                position: latLng,
                title: 'Pick Up here!'
            });

            this.marker.setMap(this.map);

            if(this.first)
            this.map.setCenter(latLng);


        } else
        {
            this.url =  " https://www.google.com/maps/dir/?api=1&origins="
                + this.customerProvider.lat + ',' + this.customerProvider.lng +
                "&destination=" + this.delivery.toLat + "," + this.delivery.toLng;

            let latLng = new google.maps.LatLng(this.delivery.toLat, this.delivery.toLat);

            let mapOptions = {
                center: latLng,
                zoom: 18,
                tilt:30,
                disableDefaultUI:false,
                mapTypeControl:false,
                zoomControl:false,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                clickableIcons:false
            };

            this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

            this.marker = new google.maps.Marker({
                position: latLng,
                title: 'Drop Off here!'
            });

            this.marker.setMap(this.map);

            if(this.first)
            this.map.setCenter(latLng);

        }

        // this.interval =  setInterval(()=>{
        //
        //     this.updateLocation();
        //     // this.updateDropperLocation();
        // },15000);

    }


    updateLocation(){
        if(this.delivery.pickedUpAt === null){
            let latLng = new google.maps.LatLng( this.delivery.fromLat , this.delivery.fromLng);


            this.marker = new google.maps.Marker({
                position: latLng,
                title: 'Pick Up here!'
            });

            this.marker.setMap(this.map);

            if(this.first)
            this.map.setCenter(latLng);


        } else
        {
            // let latLng = new google.maps.LatLng(9.070400, 7.487462);
            let latLng = new google.maps.LatLng(this.delivery.toLat, this.delivery.toLng);

            this.marker = new google.maps.Marker({
                position: latLng,
                title: 'Drop Off here!'
            });

            this.marker.setMap(this.map);
            if(this.first)
            this.map.setCenter(latLng);

        }

    }

    updateDropperLocation(){
//        console.log("Getting dropper location");
        this.customerProvider.getDropperLocation(this.delivery.dropper.drid).subscribe((success)=>{

            let myLatLng = new google.maps.LatLng(success.lat, success.lng);

            console.log(success);

            console.log(myLatLng);

            if(isUndefined(this.marker)) {
                this.marker = new google.maps.Marker({
                    position: myLatLng,
                    title: 'Your dropper is here!'
                });
                this.marker.setMap(this.map);
            }
            this.marker.setPosition(myLatLng);
            this.map.setCenter(myLatLng);
        },(error)=>{ console.log(error)});

    }

    phoneAction(phone){
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Please confirm your action',
            buttons: [
                {
                    text: 'Call',
                    handler: () => {

                        this.url = "tel: " + phone;
                        this.navigate();

                    }
                },{
                    text: 'SMS',
                    handler: () => {
                        //CONFIGURATION
                        let options = {
                            replaceLineBreaks: false, // true to replace \n by a new line, false by default
                            android: {
                                intent: 'INTENT'  // send SMS with the native android SMS messaging
                            }
                        };

                        this.sms.send( phone , '',options);
                    }
                },{
                    text: 'Whats App',
                    handler: () => {
                        this.url = "https://api.whatsapp.com/send?phone=" + phone;
                        this.navigate();
                    }
                },{
                    text: 'Close Menu',
                    role: 'cancel',
                    handler: () => {
                        //
                    }
                }
            ]
        });
        actionSheet.present();

    }



    navigate(){

        this.iab.create(this.url,"_system","location=no,hidden=yes");

    }

}
