import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { SignUpPageModule} from "../pages/sign-up/sign-up.module";
import { CustomerProvider } from '../providers/customer/customer';
import { HttpModule} from "@angular/http";
import {DashboardPageModule} from "../pages/dashboard/dashboard.module";
import {IonicStorageModule} from "@ionic/storage";
import {NewDeliveryPageModule} from "../pages/new-delivery/new-delivery.module";
import {MapPageModule} from "../pages/map/map.module";
import {GoogleMaps} from "@ionic-native/google-maps";
import { Geolocation } from '@ionic-native/geolocation';
import { AndroidPermissions } from '@ionic-native/android-permissions';
import {Camera} from "@ionic-native/camera";
import {File} from "@ionic-native/file";
import {FilePath} from "@ionic-native/file-path";
import {FileTransfer} from "@ionic-native/file-transfer";
import {ProfilePageModule} from "../pages/profile/profile.module";
import {SupportPageModule} from "../pages/support/support.module";
import { DropperProvider } from '../providers/dropper/dropper';
import {InProgressPageModule} from "../pages/in-progress/in-progress.module";
import {PaymentsPageModule} from "../pages/payments/payments.module";
import {AvailableRequestsPageModule} from "../pages/available-requests/available-requests.module";
import {SettingsPageModule} from "../pages/settings/settings.module";
import {HistoryPageModule} from "../pages/history/history.module";
import {EarningsPageModule} from "../pages/earnings/earnings.module";
import {DeliveriesPageModule} from "../pages/deliveries/deliveries.module";
import {DeliveryDetailsPageModule} from "../pages/delivery-details/delivery-details.module";
import {BackgroundGeolocation} from "@ionic-native/background-geolocation";
import {WithdrawalsPageModule} from "../pages/withdrawals/withdrawals.module";
import {AffiliatePageModule} from "../pages/affiliate/affiliate.module";
import {InAppBrowser} from "@ionic-native/in-app-browser";
import {Facebook} from "@ionic-native/facebook";
import {RequestsPageModule} from "../pages/requests/requests.module";
import {ContactsPageModule} from "../pages/contacts/contacts.module";
import {LocationsPageModule} from "../pages/locations/locations.module";
import {SMS} from "@ionic-native/sms";
import {ImagePicker} from "@ionic-native/image-picker";
import {PageGmapAutocomplete} from "../pages/page-gmap-autocomplete/page-gmap-autocomplete";
import {ModalAutocompleteItems} from "../pages/modal-autocomplete-items/modal-autocomplete-items";
import {OneSignal} from "@ionic-native/onesignal";
import {SigninemailModule} from "../pages/signinemail/signinemail.module";
import {HomePageModule} from "../pages/home/home.module";

@NgModule({
  declarations: [
      MyApp,
      PageGmapAutocomplete,
      ModalAutocompleteItems
  ],
  imports: [
      BrowserModule,
      HttpModule,
      IonicStorageModule.forRoot(),
      IonicModule.forRoot(MyApp),
      HomePageModule,
      SignUpPageModule,
      SigninemailModule,
      DashboardPageModule,
      NewDeliveryPageModule,
      ProfilePageModule,
      SupportPageModule,
      MapPageModule,
      InProgressPageModule,
      PaymentsPageModule,
      AvailableRequestsPageModule,
      SettingsPageModule,
      HistoryPageModule,
      EarningsPageModule,
      DeliveriesPageModule,
      WithdrawalsPageModule,
      DeliveryDetailsPageModule,
      AffiliatePageModule,
      RequestsPageModule,
      ContactsPageModule,
      LocationsPageModule,


  ],
  bootstrap: [IonicApp],
  entryComponents: [
      MyApp,
      PageGmapAutocomplete,
      ModalAutocompleteItems

  ],
  providers: [
      StatusBar,
      SplashScreen,
      {provide: ErrorHandler, useClass: IonicErrorHandler},
      CustomerProvider,
      GoogleMaps,
      Geolocation,
      BackgroundGeolocation,
      AndroidPermissions,
      Camera,
      File,
      FileTransfer,
      FilePath,
      InAppBrowser,
      DropperProvider,
      Facebook,
      SMS,
      ImagePicker,
      OneSignal
  ]
})
export class AppModule {}
