import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import {DashboardPage} from "../pages/dashboard/dashboard";
import {ICustomer} from "../interfaces/customer.interface";
import {ProfilePage} from "../pages/profile/profile";
import {SettingsPage} from "../pages/settings/settings";
import {CustomerProvider} from "../providers/customer/customer";

import {Storage} from "@ionic/storage";
import {OneSignal} from "@ionic-native/onesignal";


declare let Bugsnag;
@Component({
  templateUrl: 'app.html'
})

export class MyApp {
    @ViewChild(Nav) nav: Nav;

    rootPage: any = HomePage;


    customer:ICustomer;
    pages: Array<{title: string, component: any, params: any}>;
    deliveries: Array<{title: string, component: any, params: any}>;
    droppers: Array<{title: string, component: any, params: any}>;
    partners: Array<{title: string, component: any, params: any}>;

    constructor(public platform: Platform, public statusBar: StatusBar,
              public customerProvider:CustomerProvider,
              public splashScreen: SplashScreen,
                private oneSignal: OneSignal,
              public storage: Storage) {

    this.initializeApp();

    }

    ionViewDidLoad(){
        this.storage.get('customer').then((value)=>{
            this.customer = value;

            if(this.customer !== null){
                if(this.customer.role == "Dropper"){
                    this.customerProvider.showDropperMenu = true;
                    //console.log("This is a dropper so we would track");
                    this.customerProvider.startTracking();
                }

                if(this.customer.role == "Vehicle Partner"){
                    this.customerProvider.showVpMenu = true;
                }

            }

        });

        // used for an example of ngFor and navigation
        // this.deliveries = [
        //     { title: 'New Delivery', component: NewDeliveryPage, params : "{}" },
        //     { title: 'Incoming Deliveries', component: DeliveriesPage , params: {type:'incoming'} },
        //     { title: 'Outgoing Deliveries', component: DeliveriesPage, params : {'type':'outgoing'} },
        // ];

        // used for an example of ngFor and navigation
        this.pages = [
            { title: 'My Profile', component: ProfilePage, params : "{}" },
            { title: 'Settings', component: SettingsPage, params : "{}" },
            // { title: 'Support', component: SupportPage, params : "{}" },
            // { title: 'Payments', component: PaymentsPage, params : "{}" }
        ];
        // used for an example of ngFor and navigation
        // this.droppers = [
        //     { title: 'Available Requests', component: AvailableRequestsPage, params : "{}" },
        //     { title: 'In-Progress', component: InProgressPage, params : "{}" },
        //     { title: 'History', component: HistoryPage, params : "{}" },
        //     // { title: 'Vehicle Info', component: ListPage, params : "{}" }
        // ];
        // used for an example of ngFor and navigation
        // this.partners = [
        //     { title: 'Earnings', component: EarningsPage, params : "{}" },
        //     { title: 'Withdrawals', component: WithdrawalsPage, params : "{}" },
        //     // { title: 'Vehicle Info', component: ListPage, params : "{}" }
        // ];

    }

    dashboard(){
      this.nav.setRoot(DashboardPage);
    }

    logout(){
      this.storage.set('isSignedIn',false);
      this.storage.remove('customer');
      this.customerProvider.customer = null;
      setTimeout(()=>{this.customerProvider.stopTracking();},500);

      this.nav.setRoot(HomePage)
    }

    initializeApp() {

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
        this.statusBar.styleDefault();
        setTimeout(()=>{this.splashScreen.hide()}, 200);


        this.oneSignal.startInit('9589c6fe-4df9-4fbd-9701-d85bc78ea105', '553832677044');

        this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

        this.oneSignal.handleNotificationReceived().subscribe(() => {
            console.log('notification received');
            // do something when notification is received
        });

        this.oneSignal.handleNotificationOpened().subscribe(() => {
            console.log('notification opened');
            // do something when a notification is opened
        });

        this.oneSignal.endInit();
        });
    }

    openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component,page.params);
    }


}
